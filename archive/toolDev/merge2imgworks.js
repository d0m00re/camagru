const fs = require('fs')
const { createCanvas, loadImage } = require('canvas')

function saveCanvaToPngImg(canvas, pathName){
    // console.log('<img src="' + canvas.toDataURL() + '" />')
    canvas.toDataURL('image/png', (err, png) => {

    var data = png.replace(/^data:image\/\w+;base64,/, "");
    var buf = new Buffer(data, 'base64');

    fs.writeFileSync(pathName, buf)
  })
}

const canvas = createCanvas(200, 200)
const ctx = canvas.getContext('2d')

// Write "Awesome!"
ctx.font = '30px Impact'
ctx.rotate(0.1)
ctx.fillText('42!', 50, 100)

// Draw line under text
var text = ctx.measureText('Awesome!')
ctx.strokeStyle = 'rgba(1,0,0,0.5)'
ctx.beginPath()
ctx.lineTo(50, 102)
ctx.lineTo(50 + text.width, 102)
ctx.stroke()



// Draw cat with lime helmet

const loadImagePromises = []

loadImagePromises.push(loadImage('./img/cat1.png'))
loadImagePromises.push(loadImage('./img/arrow.png'))

console.log(loadImagePromises)

Promise.all(loadImagePromises).then(values => {
    console.log(values[0])
    ctx.drawImage(values[0], 50, 0, 70, 70)
    ctx.drawImage(values[1], 50, 50, 70, 70)

    saveCanvaToPngImg(canvas, './target/john.png')
})