webpackHotUpdate("main",{

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _logo_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./logo.svg */ "./src/logo.svg");
/* harmony import */ var _logo_svg__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_logo_svg__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App.css */ "./src/App.css");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_App_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _containers_Sign_Loggin_Login__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./containers/Sign/Loggin/Login */ "./src/containers/Sign/Loggin/Login.js");
/* harmony import */ var _containers_Sign_Register_Register__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./containers/Sign/Register/Register */ "./src/containers/Sign/Register/Register.js");
/* harmony import */ var _containers_Sign_SignOut_SignOut__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./containers/Sign/SignOut/SignOut */ "./src/containers/Sign/SignOut/SignOut.js");
/* harmony import */ var _containers_Gallerie_containers_ImageUpload__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./containers/Gallerie/containers/ImageUpload */ "./src/containers/Gallerie/containers/ImageUpload.js");
/* harmony import */ var _containers_Gallerie_containers_SelfieEditor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./containers/Gallerie/containers/SelfieEditor */ "./src/containers/Gallerie/containers/SelfieEditor.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _containers_Gallerie_containers_Selfie__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./containers/Gallerie/containers/Selfie */ "./src/containers/Gallerie/containers/Selfie.js");
/* harmony import */ var _containers_Gallerie_GallerieDisplay_components_CardSelfie__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./containers/Gallerie/GallerieDisplay/components/CardSelfie */ "./src/containers/Gallerie/GallerieDisplay/components/CardSelfie.js");
/* harmony import */ var _containers_Gallerie_GallerieDisplay_container_PublicGallerie__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./containers/Gallerie/GallerieDisplay/container/PublicGallerie */ "./src/containers/Gallerie/GallerieDisplay/container/PublicGallerie.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/App.js";














const mapStateToProps = function (state) {
  console.log(state);
  return {
    username: state.username,
    isConnect: state.isConnect
  };
};

function Debug(state) {
  console.log(state);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, "username : ", state.username, " | isCOnnect : ", state.isConnect));
}

function LinkShowHide(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, props.show && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    key: props.label,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    to: props.to,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, props.label)));
}

function MainLink(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "containerNavbar",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: !props.isConnect,
    to: "/login",
    label: "Login",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: !props.isConnect,
    to: "/register",
    label: "Register",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/editAccount",
    label: "Edit account",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: true,
    to: "/gallerie",
    label: "Gallery",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: true,
    to: "/SelfieEditorTest",
    label: "Editor test",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/SelfieEditor",
    label: "Selfie Editor",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/Selfie",
    label: "Selfie",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/CardSelfie",
    label: "Card Selfie",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/signOut",
    label: "Sign Out",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, " CARD SELFIE TEST"));
}

function MainRoot(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Switch"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/",
    component: Gallerie,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/login",
    component: _containers_Sign_Loggin_Login__WEBPACK_IMPORTED_MODULE_4__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/register",
    component: _containers_Sign_Register_Register__WEBPACK_IMPORTED_MODULE_5__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/editAccount",
    component: EditAccount,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/gallerie",
    component: _containers_Gallerie_GallerieDisplay_container_PublicGallerie__WEBPACK_IMPORTED_MODULE_12__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/SelfieEditorTest",
    component: () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_Gallerie_containers_SelfieEditor__WEBPACK_IMPORTED_MODULE_8__["default"], {
      apiReqDisplayImg: "http://localhost:4444/selfie/img/get/toor/1583245977966-space-cat.jpg",
      idImg: 78,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/SelfieEditor",
    component: _containers_Gallerie_containers_SelfieEditor__WEBPACK_IMPORTED_MODULE_8__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/Selfie",
    component: _containers_Gallerie_containers_Selfie__WEBPACK_IMPORTED_MODULE_10__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/CardSelfie",
    component: () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_Gallerie_GallerieDisplay_components_CardSelfie__WEBPACK_IMPORTED_MODULE_11__["default"], {
      srcImg: "https://images.unsplash.com/photo-1508138221679-760a23a2285b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
      className: "divCardSelfie",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/signOut",
    component: _containers_Sign_SignOut_SignOut__WEBPACK_IMPORTED_MODULE_6__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    component: Error404,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  })));
}
/*
function Selfie(props) {
  return (
    <>
      <h1>Selfie</h1>
    </>
  )
}
*/


function Error404(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: this
  }, "Error 404"));
}

function Gallerie(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107
    },
    __self: this
  }, "Galerie");
}

function EditAccount(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: this
  }, " Create Account"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115
    },
    __self: this
  }, " Login"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: this
  }, "username : "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: this
  }, "password : "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "password",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 121
    },
    __self: this
  }, "email : "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "password",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 122
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 123
    },
    __self: this
  })));
}

function App(props) {
  console.log(props);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "App",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 134
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["BrowserRouter"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 135
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(MainLink, {
    isConnect: props.isConnect,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 136
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(MainRoot, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Debug, {
    username: props.username,
    isConnect: props.isConnect,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 139
    },
    __self: this
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps)(App));

/***/ })

})
//# sourceMappingURL=main.ade6e6d1ca6463acad27.hot-update.js.map