(this["webpackJsonpfront"] = this["webpackJsonpfront"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/App.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".App {\n  text-align: center;\n}\n\n.App-logo {\n  height: 40vmin;\n  pointer-events: none;\n}\n\n@media (prefers-reduced-motion: no-preference) {\n  .App-logo {\n    animation: App-logo-spin infinite 20s linear;\n  }\n}\n\n.App-header {\n  background-color: #282c34;\n  min-height: 100vh;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  font-size: calc(10px + 2vmin);\n  color: white;\n}\n\n.App-link {\n  color: #61dafb;\n}\n\n@keyframes App-logo-spin {\n  from {\n    transform: rotate(0deg);\n  }\n  to {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/containers/Gallerie/GallerieDisplay/css/Card.css":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/containers/Gallerie/GallerieDisplay/css/Card.css ***!
  \*****************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".divCardSelfie{\n    clear:both;\n    width : 300px;\n    height: 300px;\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);\n}\n\n.divCardSelfie img{\n    max-width: 100%;\n    height: auto;\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/index.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "body {\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',\n    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',\n    sans-serif;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n\ncode {\n  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',\n    monospace;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./src/App.css":
/*!*********************!*\
  !*** ./src/App.css ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _logo_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./logo.svg */ "./src/logo.svg");
/* harmony import */ var _logo_svg__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_logo_svg__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App.css */ "./src/App.css");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_App_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _containers_Sign_Loggin_Login__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./containers/Sign/Loggin/Login */ "./src/containers/Sign/Loggin/Login.js");
/* harmony import */ var _containers_Sign_Register_Register__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./containers/Sign/Register/Register */ "./src/containers/Sign/Register/Register.js");
/* harmony import */ var _containers_Sign_SignOut_SignOut__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./containers/Sign/SignOut/SignOut */ "./src/containers/Sign/SignOut/SignOut.js");
/* harmony import */ var _containers_Gallerie_containers_ImageUpload__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./containers/Gallerie/containers/ImageUpload */ "./src/containers/Gallerie/containers/ImageUpload.js");
/* harmony import */ var _containers_Gallerie_containers_SelfieEditor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./containers/Gallerie/containers/SelfieEditor */ "./src/containers/Gallerie/containers/SelfieEditor.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _containers_Gallerie_containers_Selfie__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./containers/Gallerie/containers/Selfie */ "./src/containers/Gallerie/containers/Selfie.js");
/* harmony import */ var _containers_Gallerie_GallerieDisplay_components_CardSelfie__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./containers/Gallerie/GallerieDisplay/components/CardSelfie */ "./src/containers/Gallerie/GallerieDisplay/components/CardSelfie.js");
/* harmony import */ var _containers_Gallerie_GallerieDisplay_container_PublicGallerie__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./containers/Gallerie/GallerieDisplay/container/PublicGallerie */ "./src/containers/Gallerie/GallerieDisplay/container/PublicGallerie.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/App.js";














const mapStateToProps = function (state) {
  console.log(state);
  return {
    username: state.username,
    isConnect: state.isConnect
  };
};

function Debug(state) {
  console.log(state);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, "username : ", state.username, " | isCOnnect : ", state.isConnect));
}

function LinkShowHide(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, props.show && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    key: props.label,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    to: props.to,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, props.label)));
}

function MainLink(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "containerNavbar",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: !props.isConnect,
    to: "/login",
    label: "Login",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: !props.isConnect,
    to: "/register",
    label: "Register",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/editAccount",
    label: "Edit account",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: true,
    to: "/gallerie",
    label: "Gallery",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: true,
    to: "/SelfieEditorTest",
    label: "Editor test",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/SelfieEditor",
    label: "Selfie Editor",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/Selfie",
    label: "Selfie",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/CardSelfie",
    label: "Card Selfie",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkShowHide, {
    show: props.isConnect,
    to: "/signOut",
    label: "Sign Out",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, " CARD SELFIE TEST"));
}

function MainRoot(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Switch"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/",
    component: Gallerie,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/login",
    component: _containers_Sign_Loggin_Login__WEBPACK_IMPORTED_MODULE_4__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/register",
    component: _containers_Sign_Register_Register__WEBPACK_IMPORTED_MODULE_5__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/editAccount",
    component: EditAccount,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/gallerie",
    component: _containers_Gallerie_GallerieDisplay_container_PublicGallerie__WEBPACK_IMPORTED_MODULE_12__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/SelfieEditorTest",
    component: () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_Gallerie_containers_SelfieEditor__WEBPACK_IMPORTED_MODULE_8__["default"], {
      apiReqDisplayImg: "http://localhost:4444/img/selfie/get/toor/1583245977966-space-cat.jpg",
      idImg: 78,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/SelfieEditor",
    component: _containers_Gallerie_containers_SelfieEditor__WEBPACK_IMPORTED_MODULE_8__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/Selfie",
    component: _containers_Gallerie_containers_Selfie__WEBPACK_IMPORTED_MODULE_10__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/CardSelfie",
    component: () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_Gallerie_GallerieDisplay_components_CardSelfie__WEBPACK_IMPORTED_MODULE_11__["default"], {
      srcImg: "https://images.unsplash.com/photo-1508138221679-760a23a2285b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
      className: "divCardSelfie",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    exact: true,
    path: "/signOut",
    component: _containers_Sign_SignOut_SignOut__WEBPACK_IMPORTED_MODULE_6__["default"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Route"], {
    component: Error404,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  })));
}
/*
function Selfie(props) {
  return (
    <>
      <h1>Selfie</h1>
    </>
  )
}
*/


function Error404(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: this
  }, "Error 404"));
}

function Gallerie(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107
    },
    __self: this
  }, "Galerie");
}

function EditAccount(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: this
  }, " Create Account"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115
    },
    __self: this
  }, " Login"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: this
  }, "username : "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: this
  }, "password : "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "password",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 121
    },
    __self: this
  }, "email : "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "password",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 122
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    type: "button",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 123
    },
    __self: this
  })));
}

function App(props) {
  console.log(props);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "App",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 134
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["BrowserRouter"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 135
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(MainLink, {
    isConnect: props.isConnect,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 136
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(MainRoot, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Debug, {
    username: props.username,
    isConnect: props.isConnect,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 139
    },
    __self: this
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_9__["connect"])(mapStateToProps)(App));

/***/ }),

/***/ "./src/components/formUnit/Button.js":
/*!*******************************************!*\
  !*** ./src/components/formUnit/Button.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Button; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/components/formUnit/Button.js";

function Button(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    onClick: props.action,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, props.title));
}

/***/ }),

/***/ "./src/components/formUnit/Image.js":
/*!******************************************!*\
  !*** ./src/components/formUnit/Image.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Image; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/components/formUnit/Image.js";

/*
** className, src, alt
*/

function Image(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: props.className,
    src: props.src,
    alt: props.alt,
    id: props.id,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }));
}

/***/ }),

/***/ "./src/components/formUnit/Input.js":
/*!******************************************!*\
  !*** ./src/components/formUnit/Input.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Input; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/components/formUnit/Input.js";

function Input(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, props.labelText), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: props.types,
    value: props.value,
    onChange: props.onChange,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }));
}

/***/ }),

/***/ "./src/components/formUnit/NumberInput.js":
/*!************************************************!*\
  !*** ./src/components/formUnit/NumberInput.js ***!
  \************************************************/
/*! exports provided: NumberInput, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberInput", function() { return NumberInput; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/components/formUnit/NumberInput.js";

class NumberInput extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleNumberW = event => {
      this.setState({
        numberW: event.target.value
      });
    };

    this.state = {
      numberW: 2
    };
  }

  render() {
    console.log(this.state.numberW);
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, this.props.labelText), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      type: "number",
      value: this.state.numberW,
      onChange: this.handleNumberW,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22
      },
      __self: this
    }));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (NumberInput);

/***/ }),

/***/ "./src/config.js":
/*!***********************!*\
  !*** ./src/config.js ***!
  \***********************/
/*! exports provided: API_ENTRYPOINT, API_ADD_USER, API_CHECK_USER */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_ENTRYPOINT", function() { return API_ENTRYPOINT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_ADD_USER", function() { return API_ADD_USER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_CHECK_USER", function() { return API_CHECK_USER; });
const API_ENTRYPOINT = 'http://localhost:4444';
const API_ADD_USER = API_ENTRYPOINT + '/user/add';
const API_CHECK_USER = API_ENTRYPOINT + '/user/checkConnect2';

/***/ }),

/***/ "./src/containers/Gallerie/GallerieDisplay/components/CardSelfie.js":
/*!**************************************************************************!*\
  !*** ./src/containers/Gallerie/GallerieDisplay/components/CardSelfie.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_formUnit_Image__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../components/formUnit/Image */ "./src/components/formUnit/Image.js");
/* harmony import */ var _css_Card_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../css/Card.css */ "./src/containers/Gallerie/GallerieDisplay/css/Card.css");
/* harmony import */ var _css_Card_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_css_Card_css__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Gallerie/GallerieDisplay/components/CardSelfie.js";




function Like(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: props.className,
    onClick: props.onClick,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, "LIKE"));
}

function Comment(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: props.className,
    onClick: props.onClick,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, "Comment"));
}

function Share(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: props.className,
    onClick: props.onClick,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, "Comment"));
}

function CardSelfie(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: props.className,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Image__WEBPACK_IMPORTED_MODULE_1__["default"], {
    src: props.srcImg,
    alt: props.alt,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "cardContainer",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Like, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Comment, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Share, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (CardSelfie);

/***/ }),

/***/ "./src/containers/Gallerie/GallerieDisplay/container/PublicGallerie.js":
/*!*****************************************************************************!*\
  !*** ./src/containers/Gallerie/GallerieDisplay/container/PublicGallerie.js ***!
  \*****************************************************************************/
/*! exports provided: PublicGallerie, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicGallerie", function() { return PublicGallerie; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Gallerie/GallerieDisplay/container/PublicGallerie.js";

/*
** goal: display all user selfie
** 1) fetch all selfie
** 2) map with CardSelfie.js
*/


class PublicGallerie extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const reqGetAllSelfie = "http://localhost:4444/file/selfie/all";
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(reqGetAllSelfie).then(res => {
      const gallerie = res.data;
      console.log(gallerie);
    });
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, " display all userimg"));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (PublicGallerie);

/***/ }),

/***/ "./src/containers/Gallerie/GallerieDisplay/css/Card.css":
/*!**************************************************************!*\
  !*** ./src/containers/Gallerie/GallerieDisplay/css/Card.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../../../node_modules/postcss-loader/src??postcss!./Card.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/containers/Gallerie/GallerieDisplay/css/Card.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../../../node_modules/postcss-loader/src??postcss!./Card.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/containers/Gallerie/GallerieDisplay/css/Card.css", function() {
		var newContent = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../../../../../node_modules/postcss-loader/src??postcss!./Card.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/containers/Gallerie/GallerieDisplay/css/Card.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/containers/Gallerie/containers/ImageUpload.js":
/*!***********************************************************!*\
  !*** ./src/containers/Gallerie/containers/ImageUpload.js ***!
  \***********************************************************/
/*! exports provided: ImageUpload, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageUpload", function() { return ImageUpload; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_formUnit_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../components/formUnit/Button */ "./src/components/formUnit/Button.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_formUnit_Image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../components/formUnit/Image */ "./src/components/formUnit/Image.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Gallerie/containers/ImageUpload.js";






const mapStateToProps = function (state) {
  return {
    username: state.username,
    userid: state.userid,
    token: state.token
  };
};

function InputExplorerFile(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    name: props.name,
    className: props.className,
    onChange: props.onChange,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  });
}

class ImageUpload extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.initialState = () => {
      return {
        name: null,
        imagePreviewUrl: "http://localhost:4444/profile/pic/asd",
        selectedFile: null
      };
    };

    this.handleName = event => {
      this.setState({
        selectedFile: event.target.files[0]
      });
      let reader = new FileReader(); // if a valid file

      reader.onloadend = () => {
        this.setState({
          imagePreviewUrl: reader.result
        });
      };

      reader.readAsDataURL(event.target.files[0]);
      this.setState({
        link: event.target.files[0]
      });
    };

    this.handleSubmit = event => {
      event.preventDefault();
      console.log("Image upload : ");
      const data = new FormData();
      data.append('username', this.props.username);
      data.append('userId', this.props.userid);
      data.append('token', this.props.token);
      data.append('file', this.state.selectedFile); // console.log('form data json : ')

      axios__WEBPACK_IMPORTED_MODULE_2___default.a.post("http://localhost:4444/img/upload", data, {}).then(res => {
        console.log(res);
      });
    };

    this.state = this.initialState();
  }

  render() {
    console.log(this.state);
    console.log(this.props);
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: this
    }, "welcome on my image upload"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      className: "uploadImg",
      src: this.state.imagePreviewUrl,
      alt: "fruit",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(InputExplorerFile, {
      name: "imageUpload",
      className: "imageUploadContainer",
      onChange: this.handleName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
      action: this.handleSubmit,
      title: "upload",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Image__WEBPACK_IMPORTED_MODULE_4__["default"], {
      className: "img",
      src: "http://localhost:4444/img/sticker/poussin.png",
      alt: "test",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(mapStateToProps)(ImageUpload));

/***/ }),

/***/ "./src/containers/Gallerie/containers/Selfie.js":
/*!******************************************************!*\
  !*** ./src/containers/Gallerie/containers/Selfie.js ***!
  \******************************************************/
/*! exports provided: Selfie, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Selfie", function() { return Selfie; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SelfieUpload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SelfieUpload */ "./src/containers/Gallerie/containers/SelfieUpload.js");
/* harmony import */ var _SelfieEditor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SelfieEditor */ "./src/containers/Gallerie/containers/SelfieEditor.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Gallerie/containers/Selfie.js";



/*
** state = 0 : need to upload a picutre
** state = 1 : selfie edition

idImg : id of he selfie in my db
*/

class Selfie extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.changeApiReqUpdate = apiUrl => {
      this.setState({
        apiReqDisplayImg: apiUrl
      });
    };

    this.changeUploadStateToEdition = () => {
      this.setState({
        stateUpload: 1
      });
    };

    this.changeUploadStateToUpload = () => {
      this.setState({
        stateUpload: 0
      });
    };

    this.changeIdImgValue = id => {
      this.setState({
        idImg: id
      });
    };

    this.state = {
      stateUpload: 0,
      apiReqDisplayImg: null,
      idImg: -1
    };
  }

  render() {
    console.log('State : ');
    console.log(this.state);
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, " Welcome on my sefie edition"), this.state.stateUpload == 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SelfieUpload__WEBPACK_IMPORTED_MODULE_1__["default"], {
      onSubmit: this.changeUploadStateToEdition,
      handleApiReqUpdate: this.changeApiReqUpdate,
      handleIdImgValueUpdate: this.changeIdImgValue,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: this
    }), this.state.stateUpload == 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SelfieEditor__WEBPACK_IMPORTED_MODULE_2__["SelfieEditor"], {
      onSubmit: this.changeUploadStateToUpload,
      apiReqDisplayImg: this.state.apiReqDisplayImg,
      idImg: this.state.idImg,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Selfie);

/***/ }),

/***/ "./src/containers/Gallerie/containers/SelfieEditor.js":
/*!************************************************************!*\
  !*** ./src/containers/Gallerie/containers/SelfieEditor.js ***!
  \************************************************************/
/*! exports provided: SelfieEditor, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfieEditor", function() { return SelfieEditor; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_formUnit_Input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../components/formUnit/Input */ "./src/components/formUnit/Input.js");
/* harmony import */ var _components_formUnit_NumberInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../components/formUnit/NumberInput */ "./src/components/formUnit/NumberInput.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Gallerie/containers/SelfieEditor.js";
//https://node-postgres.com/features/queries#parameterized-query






const mapStateToProps = function (state) {
  return {
    username: state.username,
    userid: state.userid,
    token: state.token
  };
};

class SelfieEditor extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.initialState = () => {
      return {
        name: null,
        selectedFile: null,
        selfieApiUrl: null,
        selfieApiUrlValid: false,
        dimOri: {
          h: 500,
          w: 500
        },
        idStickerSelect: -1,
        dim: {
          h: 100,
          w: 100
        },
        pos: {
          x: 0,
          y: 0
        },
        imgReqApi: null,
        idSelfie: null,
        clickImg: false,
        selfieTmpUrl: null,
        dimImgClient: {
          h: 0,
          w: 0
        },
        key: "?n=" + Date.now()
      };
    };

    this.getPosClick = event => {
      this.setState({
        pos: {
          x: event.offsetX,
          y: event.offsetY
        }
      });
      const reqMerge = {
        'idSelfie': this.props.idImg,
        'idStickerSelect': this.state.idStickerSelect,
        'setting': {
          'imgOri': {
            'dim': this.state.dimOri
          },
          'imgAdd': {
            'dim': this.state.dim,
            'pos': {
              'x': event.offsetX,
              'y': event.offsetY
            }
          }
        }
      };
      console.log('req merge : ');
      console.log(reqMerge);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("http://localhost:4444/img/merge", reqMerge, {}).then(res => {
        console.log(res);
        this.setState({
          key: "?n=" + Date.now()
        });
      });
    };

    this.handleIdStickerSelect = (index, event) => {
      console.log(`Change sticker id : ${index}`);
      this.setState({
        idStickerSelect: index
      });
    };

    this.handleChangeDimWidthSticker = event => {
      let cpImgAdd = this.state.dim;
      cpImgAdd.w = parseInt(event.target.value);
      this.setState({
        dim: cpImgAdd
      });
    };

    this.handleChangeDimHeightSticker = event => {
      let cpImgAdd = this.state.dim;
      cpImgAdd.h = parseInt(event.target.value);
      this.setState({
        dim: cpImgAdd
      });
    };

    this.onImgLoad = ({
      target: img
    }) => {
      this.setState({
        dimImgClient: {
          height: img.offsetHeight,
          width: img.offsetWidth
        }
      });
    };

    this.state = this.initialState();
  }
  /*
  ** clickImg : if the user click on the image
  */


  //http://localhost:4444/img/stickers
  // get all sticker from bdd
  componentDidMount() {
    const reqGetStickers = "http://localhost:4444/img/stickers/";
    const reqGetOneSticker = "http://localhost:4444/img/sticker/"; // add event listener : get pos clicm on our current img

    var elem = document.getElementById("selfieImg6");
    elem.addEventListener("click", this.getPosClick); //console.log(elem.se)
    //console.log(elem.clientWidth)
    // elem

    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(reqGetStickers).then(res => {
      const sticker = res.data;

      for (let i in sticker) sticker[i].imgpath = reqGetOneSticker + sticker[i].imgname;

      console.log(sticker);
      this.setState({
        selfieApiUrl: sticker,
        selfieApiUrlValid: true
      });
      console.log(sticker[0].imgpath);
    });
    this.setState({
      key: "?n=" + Date.now()
    });
  }

  componentWillUnmount() {} // var elem = document.getElementById("selfieImg")
  // elem.removeEventListener("click", this.getPosClick)

  /*
      update img with id of the table stickers
  */


  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 146
      },
      __self: this
    }, "Selfie editor"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      width: this.state.dimOri.w,
      height: this.state.dimOri.h,
      id: "selfieImg6",
      className: "uploadImg",
      src: this.props.apiReqDisplayImg + this.state.key,
      alt: "selfie",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147
      },
      __self: this
    }), this.state.selfieApiUrlValid && this.state.selfieApiUrl.map(dataSticker => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      type: "button",
      onClick: this.handleIdStickerSelect.bind(this, dataSticker.stickerid),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: dataSticker.imgpath,
      width: "50",
      height: "50",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_3__["default"], {
      types: "number",
      labelText: "Widht",
      value: this.state.dim.w,
      onChange: this.handleChangeDimWidthSticker,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_3__["default"], {
      types: "number",
      labelText: "Height",
      value: this.state.dim.h,
      onChange: this.handleChangeDimHeightSticker,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_NumberInput__WEBPACK_IMPORTED_MODULE_4__["default"], {
      labelText: "Width : ",
      className: "miaou",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 162
      },
      __self: this
    }, " offstX : ", this.state.pos.x, " | offset y : ", this.state.pos.y, " | sticker : ", this.state.idStickerSelect, " | id selfie : ", this.state.idSelfie));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(SelfieEditor));

/***/ }),

/***/ "./src/containers/Gallerie/containers/SelfieUpload.js":
/*!************************************************************!*\
  !*** ./src/containers/Gallerie/containers/SelfieUpload.js ***!
  \************************************************************/
/*! exports provided: SelfieUpload, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelfieUpload", function() { return SelfieUpload; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _components_formUnit_Image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../components/formUnit/Image */ "./src/components/formUnit/Image.js");
/* harmony import */ var _components_formUnit_Button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../components/formUnit/Button */ "./src/components/formUnit/Button.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Gallerie/containers/SelfieUpload.js";
//https://node-postgres.com/features/queries#parameterized-query






function InputExplorerFile(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "file",
    name: props.name,
    className: props.className,
    onChange: props.onChange,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  });
}

const mapStateToProps = function (state) {
  return {
    username: state.username,
    userid: state.userid,
    token: state.token
  };
};

class SelfieUpload extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.initialState = () => {
      return {
        name: null,
        imagePreviewUrl: "http://localhost:4444/profile/pic/asd",
        selectedFile: null,
        selfieApiUrl: null,
        selfieApiUrlValid: false
      };
    };

    this.handleName = event => {
      let reader = new FileReader();
      this.setState({
        selectedFile: event.target.files[0]
      }); // if a valid file

      reader.onloadend = () => {
        this.setState({
          imagePreviewUrl: reader.result
        });
      };

      reader.readAsDataURL(event.target.files[0]);
      this.setState({
        link: event.target.files[0]
      });
    };

    this.handleSubmit = event => {
      const data = new FormData();
      event.preventDefault();
      console.log("Image upload : ");
      data.append('username', this.props.username);
      data.append('userid', this.props.userid);
      data.append('token', this.props.token);
      data.append('file', this.state.selectedFile);
      console.log("display props : ");
      console.log(this.props);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("http://localhost:4444/img/upload", data, {}).then(res => {
        console.log(`---------------`);
        console.log(res.data);
        this.setState({
          //imgReqApi : res.data.apiReq,
          imagePreviewUrl: res.data.apiReq
        });
        console.log(res.data);
        this.props.handleApiReqUpdate(res.data.apiReq);
        this.props.handleIdImgValueUpdate(res.data.selfieid);
        if (this.props.onSubmit) this.props.onSubmit();
      });
    };

    this.state = this.initialState();
  }
  /*
  ** clickImg : if the user click on the image
  */


  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, "welcome on my image upload"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Image__WEBPACK_IMPORTED_MODULE_3__["default"], {
      key: this.state.key,
      id: "selfieImg",
      className: "uploadImg",
      src: this.state.imagePreviewUrl,
      alt: "fruit",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(InputExplorerFile, {
      name: "imageUpload",
      className: "imageUploadContainer",
      onChange: this.handleName,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Button__WEBPACK_IMPORTED_MODULE_4__["default"], {
      action: this.handleSubmit,
      title: "upload",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(SelfieUpload));

/***/ }),

/***/ "./src/containers/Sign/Loggin/Login.js":
/*!*********************************************!*\
  !*** ./src/containers/Sign/Loggin/Login.js ***!
  \*********************************************/
/*! exports provided: Login, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Login", function() { return Login; });
/* harmony import */ var _components_formUnit_Input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../../components/formUnit/Input */ "./src/components/formUnit/Input.js");
/* harmony import */ var _components_formUnit_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../components/formUnit/Button */ "./src/components/formUnit/Button.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _redux_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../redux/redux */ "./src/redux/redux.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../../config */ "./src/config.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Sign/Loggin/Login.js";







/*const mapStateToProps = function (state) {
    return ({
        username : state.username,
        userid : state.userid
    })
}*/

const mapDispatchToProps = dispatch => {
  return {
    storeUserInfo: user => dispatch(Object(_redux_redux__WEBPACK_IMPORTED_MODULE_4__["actionStoreUserInfo"])({
      username: user.username,
      userid: user.userid
    }))
  };
};

class Login extends react__WEBPACK_IMPORTED_MODULE_2__["Component"] {
  constructor(props) {
    super(props);

    this.clearState = () => {
      this.setState({
        username: '',
        password: ''
      });
    };

    this.handleUsername = event => {
      this.setState({
        username: event.target.value
      });
    };

    this.handlePassword = event => {
      this.setState({
        password: event.target.value
      });
    };

    this.handleSubmit = event => {
      event.preventDefault();
      console.log(`Submit : {${this.state.username}, ${this.state.password}}`);
      const userInfo = {
        username: this.state.username,
        password: this.state.password
      }; // axios request

      console.log('john snow');
      axios__WEBPACK_IMPORTED_MODULE_6___default.a.post(_config__WEBPACK_IMPORTED_MODULE_5__["API_CHECK_USER"], userInfo).then(res => {
        console.log('Call mapDispatch function storeUserInfo');
        console.log(res.data);
        this.props.storeUserInfo({
          username: res.data.username,
          userid: res.data.userId
        });
      });
    };

    this.state = {
      username: '',
      password: ''
    };
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("form", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_0__["default"], {
      types: "text",
      labelText: "username : ",
      value: this.state.username,
      onChange: this.handleUsername,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_0__["default"], {
      types: "password",
      labelText: "password : ",
      value: this.state.password,
      onChange: this.handlePassword,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_formUnit_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
      action: this.handleSubmit,
      title: "submit",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: this
    })));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(null, mapDispatchToProps)(Login));

/***/ }),

/***/ "./src/containers/Sign/Register/Register.js":
/*!**************************************************!*\
  !*** ./src/containers/Sign/Register/Register.js ***!
  \**************************************************/
/*! exports provided: Register, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Register", function() { return Register; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_formUnit_Input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../components/formUnit/Input */ "./src/components/formUnit/Input.js");
/* harmony import */ var _components_formUnit_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../components/formUnit/Button */ "./src/components/formUnit/Button.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../config */ "./src/config.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Sign/Register/Register.js";





class Register extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleUsername = event => {
      this.setState({
        username: event.target.value
      });
    };

    this.handlePassword = event => {
      this.setState({
        password: event.target.value
      });
    };

    this.handlePasswordConfirm = event => {
      this.setState({
        passwordConfirm: event.target.value
      });
    };

    this.handleEmail = event => {
      this.setState({
        email: event.target.value
      });
    };

    this.handleSubmit = event => {
      event.preventDefault();
      console.log(`Submit : {${this.state.username}, ${this.state.password}}`); // generate json

      const newUser = {
        password: this.state.password,
        username: this.state.username,
        email: this.state.email
      }; // send data on our server

      if (this.state.passwordConfirm.localeCompare(this.state.password) === 0) {
        axios__WEBPACK_IMPORTED_MODULE_4___default.a.post(_config__WEBPACK_IMPORTED_MODULE_3__["API_ADD_USER"], newUser).then(res => console.log(res));
        this.clearState();
      } else {
        this.setState({
          error: 1
        });
      }
    };

    this.clearState = () => {
      this.setState({
        password: '',
        passwordConfirm: '',
        username: '',
        email: '',
        error: 0
      });
    };

    this.state = {
      password: '',
      passwordConfirm: '',
      username: '',
      email: '',
      error: 0
    };
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_1__["default"], {
      types: "text",
      labelText: "username : ",
      value: this.state.username,
      onChange: this.handleUsername,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_1__["default"], {
      types: "password",
      labelText: "password : ",
      value: this.state.password,
      onChange: this.handlePassword,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_1__["default"], {
      types: "password",
      labelText: "repeat password : ",
      value: this.state.passwordConfirm,
      onChange: this.handlePasswordConfirm,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Input__WEBPACK_IMPORTED_MODULE_1__["default"], {
      types: "email",
      labelText: "email : ",
      value: this.state.email,
      onChange: this.handleEmail,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Button__WEBPACK_IMPORTED_MODULE_2__["default"], {
      action: this.handleSubmit,
      title: "submit",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    })));
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Register);

/***/ }),

/***/ "./src/containers/Sign/SignOut/SignOut.js":
/*!************************************************!*\
  !*** ./src/containers/Sign/SignOut/SignOut.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_formUnit_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../components/formUnit/Button */ "./src/components/formUnit/Button.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _redux_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../redux/redux */ "./src/redux/redux.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/containers/Sign/SignOut/SignOut.js";





const mapDispatchToProps = dispatch => {
  return {
    actionSignOut: () => dispatch(Object(_redux_redux__WEBPACK_IMPORTED_MODULE_3__["actionSignOut"])())
  };
};

function SignOut(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, "koukou"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_formUnit_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    action: props.actionSignOut,
    title: "Sign Out",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(null, mapDispatchToProps)(SignOut));

/***/ }),

/***/ "./src/index.css":
/*!***********************!*\
  !*** ./src/index.css ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _redux_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./redux/redux */ "./src/redux/redux.js");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./index.css */ "./src/index.css");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_index_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./App */ "./src/App.js");
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
/* harmony import */ var redux_persist_integration_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! redux-persist/integration/react */ "./node_modules/redux-persist/es/integration/react.js");
var _jsxFileName = "/Users/alhelson/web/camagru/front/src/index.js";






 //ReactDOM.render(<App />, document.getElementById('root'));


const rootElement = document.getElementById('root');
react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_2__["Provider"], {
  store: _redux_redux__WEBPACK_IMPORTED_MODULE_3__["store"],
  __source: {
    fileName: _jsxFileName,
    lineNumber: 17
  },
  __self: undefined
}, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(redux_persist_integration_react__WEBPACK_IMPORTED_MODULE_7__["PersistGate"], {
  loading: null,
  persistor: _redux_redux__WEBPACK_IMPORTED_MODULE_3__["persistor"],
  __source: {
    fileName: _jsxFileName,
    lineNumber: 18
  },
  __self: undefined
}, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_App__WEBPACK_IMPORTED_MODULE_5__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 19
  },
  __self: undefined
}))), rootElement); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_6__["unregister"]();

/***/ }),

/***/ "./src/logo.svg":
/*!**********************!*\
  !*** ./src/logo.svg ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo.5d5d9eef.svg";

/***/ }),

/***/ "./src/redux/redux.js":
/*!****************************!*\
  !*** ./src/redux/redux.js ***!
  \****************************/
/*! exports provided: actionStoreUserInfo, actionSignOut, actionStoreLastImgPath, store, persistor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actionStoreUserInfo", function() { return actionStoreUserInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actionSignOut", function() { return actionSignOut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actionStoreLastImgPath", function() { return actionStoreLastImgPath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "store", function() { return store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "persistor", function() { return persistor; });
/* harmony import */ var redux_persist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux-persist */ "./node_modules/redux-persist/es/index.js");
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-persist/lib/storage */ "./node_modules/redux-persist/lib/storage/index.js");
/* harmony import */ var redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1__);



const redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");

const createStore = redux.createStore;
const ADD_RANDOM = 'ADD_RANDOM';
const STORE_USER_INFO = 'STORE_USER_INFO';
const ACTION_SIGN_OUT = 'ACTION_SIGN_OUT';
const STORE_LAST_IMG_PATH = 'STORE_LAST_IMG_PATH';
const initialState = {
  username: '',
  userid: '',
  isConnect: false,
  error: '',
  token: '',
  randomTest: '',
  lastImagePath: '',
  key: 'root',
  storage: (redux_persist_lib_storage__WEBPACK_IMPORTED_MODULE_1___default())
};

const actionAddRandom = ran => {
  return {
    type: ADD_RANDOM,
    payload: ran
  };
};

const actionStoreUserInfo = user => {
  return {
    type: STORE_USER_INFO,
    payload: user
  };
};
const actionSignOut = () => {
  console.log('Sign out ...');
  return {
    type: ACTION_SIGN_OUT
  };
};
const actionStoreLastImgPath = imgPath => {
  return {
    type: STORE_LAST_IMG_PATH,
    payload: imgPath
  };
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_RANDOM:
      console.log("RANDOM");
      return { ...state,
        randomTest: action.payload.miaou
      };

    case STORE_USER_INFO:
      console.log("add user info");
      console.log(action.payload);
      console.log(state);
      return { ...state,
        username: action.payload.username,
        userid: action.payload.userid,
        isConnect: true,
        token: 'futureFeature'
      };

    case STORE_LAST_IMG_PATH:
      return { ...state,
        lastImagePath: action.payload.lastImagePath
      };

    case ACTION_SIGN_OUT:
      return { ...state,
        username: '',
        userid: -1,
        isConnect: false,
        token: ''
      };

    default:
      return { ...state
      };
  }
};

const persistedReducer = Object(redux_persist__WEBPACK_IMPORTED_MODULE_0__["persistReducer"])(initialState, reducer); //export const store = createStore(reducer)
//export default () => {

let store = createStore(persistedReducer);
let persistor = Object(redux_persist__WEBPACK_IMPORTED_MODULE_0__["persistStore"])(store); //  return { store, persistor }
// }

/*
store.subscribe(() => {console.log(store.getState())})

store.dispatch(actionAddRandom({miaou : 'koukou'}))
store.dispatch(actionStoreUserInfo({username: 'natridge',
                                    userid : 1}))
                                    */

/***/ }),

/***/ "./src/serviceWorker.js":
/*!******************************!*\
  !*** ./src/serviceWorker.js ***!
  \******************************/
/*! exports provided: register, unregister */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregister", function() { return unregister; });
// This optional code is used to register a service worker.
// register() is not called by default.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.
// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA
const isLocalhost = Boolean(window.location.hostname === 'localhost' || // [::1] is the IPv6 localhost address.
window.location.hostname === '[::1]' || // 127.0.0.0/8 are considered localhost for IPv4.
window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));
function register(config) {
  if (false) {}
}

function registerValidSW(swUrl, config) {
  navigator.serviceWorker.register(swUrl).then(registration => {
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;

      if (installingWorker == null) {
        return;
      }

      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the updated precached content has been fetched,
            // but the previous service worker will still serve the older
            // content until all client tabs are closed.
            console.log('New content is available and will be used when all ' + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.'); // Execute callback

            if (config && config.onUpdate) {
              config.onUpdate(registration);
            }
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.'); // Execute callback

            if (config && config.onSuccess) {
              config.onSuccess(registration);
            }
          }
        }
      };
    };
  }).catch(error => {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl, config) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl, {
    headers: {
      'Service-Worker': 'script'
    }
  }).then(response => {
    // Ensure service worker exists, and that we really are getting a JS file.
    const contentType = response.headers.get('content-type');

    if (response.status === 404 || contentType != null && contentType.indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(registration => {
        registration.unregister().then(() => {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl, config);
    }
  }).catch(() => {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    }).catch(error => {
      console.error(error.message);
    });
  }
}

/***/ }),

/***/ 1:
/*!**************************************************************************************************************!*\
  !*** multi (webpack)/hot/dev-server.js ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/alhelson/web/camagru/front/node_modules/webpack/hot/dev-server.js */"./node_modules/webpack/hot/dev-server.js");
__webpack_require__(/*! /Users/alhelson/web/camagru/front/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /Users/alhelson/web/camagru/front/src/index.js */"./src/index.js");


/***/ })

},[[1,"runtime-main",1]]]);
//# sourceMappingURL=main.chunk.js.map