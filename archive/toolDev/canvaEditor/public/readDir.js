const fs = require('fs');

export function getFileDirPromise(path){
    return new Promise(function(resolve, reject) {
        fs.readdir(path, function(err, filenames){
            if (err)
            {
                console.log('error')
                reject(err)
            }
            else
            {
                console.log('success')
                for (let i in filenames)
                {
                     filenames[i] = path + filenames[i]
                }
                resolve(filenames)
            }
        })
    })
}