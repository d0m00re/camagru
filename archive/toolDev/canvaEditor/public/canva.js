/*
** auto redim of the caneva size width
** add sticker selection
** add moore filter
** improove filter management
*/



// canva element
// main canva (editor)
var canva = null
var ctxCanva = null

var canvaVisualisator = null
var ctxVisualisator = null

//sticker element
var percentFilter = 0.0
var dimSticker = {w:150, h:150}
var sticker = null
var stickerPath = []
var stickerTab = []
let idStickerSelect = -1

var dimImg = {w : 150, h : 150}


var imgLoad = false // indict if the image is load or not
var posClick = {x : 0, y: 0}
var filter = ''; // filter list applied

/*
**
*/


// manage sticker stickerTab structure
// {img : , load : false/true}
// change this function if we want import stickers
// pathSticker tab is contain in 'pathSticker.js'

function loadStickerFromFile(path){

    let tmpImg

    for (let i in pathSticker)
    {
        tmpImg = new Image()
        stickerTab.push({img : tmpImg, load : false})
        tmpImg.src = pathSticker[i]
        tmpImg.onload = function(){
            stickerTab[i].load = true
        }
    }
}

//<input type="image" id="image" alt="Login" src="/media/examples/login-button.png">

function stickerModifDOM(){
    const stickerDOM = document.getElementsByClassName("stickerList")

    for (let i in stickerTab){
            var btn = document.createElement("INPUT");
            btn.type = 'image'
            btn.src = pathSticker[i] 
            btn.onclick = () => {updateStickerId(i);}
            stickerDOM[0].appendChild(btn)
    }
}
//


function getDimSticker(){
    return dimImg.w
}

function importSticker(path) {
    var img = new Image();

    img.src = path
    return (img)
}

function initLoadSticker(){
    const path = `../image/sticker/`
    const names = ['christmas.png' ,'poussin.png' , 'rotten-tomatoes.png', 'simplybuilt.png', 'snake.png', 'terrarium.png']

   // loadStickerFromFile('../image/sticker')
    //
    sticker = importSticker(path + names[0]);


    //ctxVisualisator.drawImage(sticker, 0, 0, dimSticker.w, dimSticker.h)
    importImgImp(ctxVisualisator, path + names[0], dimImg.w, dimImg.h)
}


function initVisualisator(){
    loadStickerFromFile('fuck/path')
    stickerModifDOM()
}

function initMainCanva() {
    canva = document.getElementById("visu");
    canvaVisualisator = document.getElementById("visuSticker")

    ctxCanva = canva.getContext("2d");
    ctxVisualisator = canvaVisualisator.getContext("2d")

    // modif the dimension of visualisator canava
    canvaVisualisator.height = dimSticker.h
    canvaVisualisator.width = dimSticker.w

   // initLoadSticker()

   // stickerModifDOM()

    canva.addEventListener("click", getPosClick)
  }

  function initCanvaEditor(){
    initMainCanva()
    initVisualisator()
    initLoadSticker()
  }

  //-------------------------------------------------------------------

//update visualisator

/*
** event on canva
*/
getPosClick = (event) => {
      posClick.x = event.offsetX
      posClick.y = event.offsetY

      var setting = {id : idStickerSelect,pos : posClick, dim : dimSticker}
      ctxCanva.filter = generateFilter(filter)
      drawImgSticker(setting)
      filter = '';
      ctxCanva.filter = 'none';
      // update visualisator
      updateVisualisator()
}

const updateVisualisator = () => {
    ctxVisualisator.clearRect(0, 0, canvaVisualisator.width, canvaVisualisator.height);

    canvaVisualisator.height = dimSticker.h
    canvaVisualisator.width = dimSticker.w

    ctxVisualisator.filter = filter
   ctxVisualisator.drawImage(stickerTab[idStickerSelect].img, 0, 0, dimSticker.w, dimSticker.h)
    ctxVisualisator.filter = 'none'
}

//---------------------------------------------------------------------------------------

const inputNumberCheckerInt = (value, vmin, vmax) => {
    vmin = parseInt(vmin)
    vmax = parseInt(vmax)
    if (value > vmax)
        value = vmax
    else if (value < vmin)
        value = vmin
    return (value)
}

const inputNumberCheckerFloat = (value, vmin, vmax) => {
    vmin = parseFloat(vmin)
    vmax = parseFloat(vmax)
    if (value > vmax)
        value = vmax
    else if (value < vmin)
        value = vmin
    return (value)
}

// update dimension of the sticker
const updateStickerHeight = (event) => {
    dimSticker.h = inputNumberCheckerInt(event.valueAsNumber,
        event.vmin,
        event.vmax)
    //update visualisator
    updateVisualisator();
}

const updateStickerWidth = (event) => {
    dimSticker.w = inputNumberCheckerInt(event.valueAsNumber,
                                        event.vmin,
                                        event.vmax)
    //update visualisator
    updateVisualisator();
}

const updateFilterPercent = (event) => {
    percentFilter = inputNumberCheckerFloat(event.valueAsNumber,
                                            event.vmin,
                                            event.vmax)
}

const updateStickerId = (event) => {
    idStickerSelect = event
    updateVisualisator();
}

//------------------------------

/*
** {dim : {w:5,h:5}, pos : {x : 10, y : 10}}
*/
function drawImg(setting) {
    ctxCanva.drawImage(sticker, setting.pos.x, setting.pos.y,
                            setting.dim.w, setting.dim.h)
}

function drawImgSticker(setting) {
    const id = parseInt(setting.id)
    ctxCanva.drawImage(stickerTab[id].img, setting.pos.x, setting.pos.y,
        setting.dim.w, setting.dim.h)
}


//-----------------------------------------------------------

  function importImgImp(c, path, w, h) {
    var img = new Image();

    img.src = path
    img.onload = function(){
        c.drawImage(img,  0, 0, w, h)
    }
  }

  function canvaRotate(deg){
    ctxCanva.rotate(deg);
  }

  function saveImg(path){
    //var dataURL = canvas.toDataURL('image/jpg', 1.0);
    canva.toBlob(function(blob) {
        saveAs(blob, path);
    });
  }

  function drawWhenLoad()
  {
    document.addEventListener('DOMContentLoaded',initCanvaEditor, false);
  }

drawWhenLoad()