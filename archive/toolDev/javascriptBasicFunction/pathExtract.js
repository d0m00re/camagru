const path = './img/selfie/miaou/1583423772237-complexicty.png'

const extractEndPath = (path) => {
    const pathExtract = path.split('/')
    const len = pathExtract.length

    if (len < 2)
        return (null)

    return (pathExtract[len - 2] + '/' + pathExtract[len - 1])
}

console.log(extractEndPath(path))
console.log(extractEndPath('ddd'))