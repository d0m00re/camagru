CREATE TABLE IF NOT EXISTS MYUSER(
    userId SERIAL PRIMARY KEY,
    email VARCHAR(50) NOT NULL UNIQUE,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL);

CREATE TABLE IF NOT EXISTS SELFIE (
    selfieid SERIAL PRIMARY KEY,
    userId INTEGER,
    imgPath varchar(150),
    imgName varchar(50),
    FOREIGN KEY (userId) REFERENCES myuser(userId));

CREATE TABLE IF NOT EXISTS COMMENTSF (
    commentid SERIAL PRIMARY KEY,
    userId INTEGER,
    selfieid INTEGER,
    FOREIGN KEY (userid) REFERENCES myuser(userid),
    FOREIGN KEY (selfieid) REFERENCES Selfie(selfieid),
    comment VARCHAR(250) NOT NULL,
    date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE IF NOT EXISTS LIKESF (
    likeid SERIAL PRIMARY KEY,
    userid INTEGER,
    selfieid INTEGER,
    FOREIGN KEY (userid) REFERENCES myuser(userid),
    FOREIGN KEY (selfieid) REFERENCES Selfie(selfieid),
    CONSTRAINT UC_LIKE UNIQUE (userid, selfieid));

CREATE TABLE IF NOT EXISTS STICKER (
    StickerId SERIAL PRIMARY KEY,
    StickerAuth integer,
    ImgPath varchar(150) UNIQUE,
    ImgName varchar(50)
);

-- import sticker
INSERT INTO Sticker (StickerAuth, ImgPath, ImgName)
VALUES (-1, './img/sticker/christmas.png', 'christmas.png');

INSERT INTO Sticker (StickerAuth, ImgPath, ImgName)
VALUES (-1, './img/sticker/poussin.png', 'poussin.png');

INSERT INTO Sticker (StickerAuth, ImgPath, ImgName)
VALUES (-1, './img/sticker/rotten-tomatoes.png', 'rotten-tomatoes.png');

INSERT INTO Sticker (StickerAuth, ImgPath, ImgName)
VALUES (-1, './img/sticker/simplybuilt.png', 'simplybuilt.png');

INSERT INTO Sticker (StickerAuth, ImgPath, ImgName)
VALUES (-1, './img/sticker/snake.png', 'snake.png');

INSERT INTO Sticker (StickerAuth, ImgPath, ImgName)
VALUES (-1, './img/sticker/terrarium.png', 'terrarium.png');

-- display sticker add
SELECT * FROM sticker;