import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import axios from 'axios'

const thunkMiddleware = require('redux-thunk').default
const redux = require('redux')

const createStore = redux.createStore

const STORE_USER_INFO = 'STORE_USER_INFO';
const ACTION_SIGN_OUT = 'ACTION_SIGN_OUT';
const STORE_LAST_IMG_PATH = 'STORE_LAST_IMG_PATH';
const ACTION_LOGIN = 'ACTION_LOGIN';
const ACTION_FAILURE_LOGIN = 'ACTION_FAILURE_LOGIN';

const initialState = {
    username: '',
    userid: '',
    isConnect: false,
    errorMsg: '',
    errorLoggin: false,
    token: '',
    randomTest: '',
    lastImagePath: ''
}

/*
** state persist when refresh page
*/
const persistedConfig = {
    key : ['root'],
    storage : storage,
    blacklist : ['errorMsg', 'errorLoggin']
}

export const actionStoreUserInfo = user => {
    return {
        type: STORE_USER_INFO,
        payload: user
    }
}

export const actionSignOut = () => {
    return {
        type: ACTION_SIGN_OUT
    }
}

export const actionStoreLastImgPath = imgPath => {
    return {
        type: STORE_LAST_IMG_PATH,
        payload: imgPath
    }
}

export const actionLogin = userInfo => {
    return {
        type: ACTION_LOGIN,
        payload: userInfo
    }
}

export const actionFailureLogin = payload => {
    return {
        type: ACTION_FAILURE_LOGIN,
        payload
    }
}

export const fetchTryConnect = (payload) => {
    return function (dispatch) {
        console.log('Try connection: ')
        console.log(payload.payload.username)
        console.log(payload.payload.password)
        axios.post('http://localhost:4444/user/checkConnect2', { username: payload.payload.username, password: payload.payload.password })
            .then(response => {
                if (response.data.valid) {
                    const dataUser = response.data
                    store.dispatch(actionStoreUserInfo(dataUser))
                } else {
                    store.dispatch(actionFailureLogin({ msg: 'fail connection' }))
                }

            })
            .catch(error => {
                console.log('failed : fetchTryConnect')
                console.log(error)
                store.dispatch(actionFailureLogin({ msg: 'fail connection' }))
            })
    }
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_LOGIN:
            console.log("redux : store_user_info")
            console.log(action.payload)
            // redux thunk check
            // if success : generate token and other info and return it

            return {
                ...state,
                username: action.payload.username,
                userid: action.payload.userId,
                isConnect: true,
                token: action.payload.token,
                errorLogin: false
            }
        case ACTION_FAILURE_LOGIN:
            console.log('failure loggin on the app')

            return {
                ...state,
                errorMsg: 'Login/Password wrong',
                errorLoggin: true
            }
        case STORE_USER_INFO:
            console.log("------ redux : store_user_info")
            console.log(action.payload)
            return {
                ...state,
                username: action.payload.username,
                userid: action.payload.userId,
                isConnect: true,
                token: action.payload.token
            }
        case STORE_LAST_IMG_PATH:
            console.log("redux : store_last_img_path")
            return {
                ...state,
                lastImagePath: action.payload.lastImagePath
            }
        case ACTION_SIGN_OUT:
            console.log("redux : action_sign_out")
            return {
                ...state,
                username: '',
                userid: -1,
                isConnect: false,
                token: ''
            }
        default:
            return { ...state }
    }
}

const persistedReducer = persistReducer(persistedConfig, reducer)

export let store = createStore(persistedReducer, redux.applyMiddleware(thunkMiddleware))
export let persistor = persistStore(store)