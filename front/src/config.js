export const API_ENTRYPOINT = 'http://localhost:4444'

/*
** USER API ROOT
*/

const USER = '/user/'
export const API_ADD_USER =       API_ENTRYPOINT + USER + 'add'
// get /http://localhost:4444/user/userid/1
export const API_GET_USER_WT_ID = API_ENTRYPOINT + USER + 'userid/'

//http://localhost:4444/user/user/john
export const API_GET_USER_WT_USERNAME = API_ENTRYPOINT + USER + 'user/'

export const API_CHECK_USER = API_ENTRYPOINT + '/user/checkConnect2'
export const API_REGISTER = API_ENTRYPOINT + '/user/register'

/*
** SELFIE API ROOT
*/
 

/*
** COMMENT API ROOT
*/

export const API_POST_COMMENT = API_ENTRYPOINT + '/comment/add'

/*
** OTHER
*/