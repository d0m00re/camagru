const port = 4444
const API_ENTRYPOINT = `http://localhost:${port}`
const API_ADD_USER = API_ENTRYPOINT + '/user/add'

// GET USER LIKE ON A SPECIFIC SELFIE
//  /selfie/get/likes/:selfieid/:userid
const API_GET_LIKE = `${API_ENTRYPOINT}/selfie/get/likes`

//IMG ANAGEMENT
// upload an image
const API_IMG_UPLOAD = `${API_ENTRYPOINT}/img/upload`// upload an image the first time
const API_IMG_UPDATE = `${API_ENTRYPOINT}/img/update`// update an image

const API_GET_ALL_STICKER = `${API_ENTRYPOINT}/img/stickers/`
const API_GET_ONE_STICKER_IMG = `${API_ENTRYPOINT}/img/sticker/`
const API_POST_MERGE_IMG = `${API_ENTRYPOINT}/img/merge/`

module.exports = {
    API_ADD_USER,
    API_ENTRYPOINT,
    API_IMG_UPLOAD,
    API_GET_LIKE,
    API_GET_ALL_STICKER,
    API_GET_ONE_STICKER_IMG,
    API_POST_MERGE_IMG,
    API_IMG_UPDATE
}