//https://node-postgres.com/features/queries#parameterized-query
import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import Image from './../../../components/formUnit/Image'
import Button from "./../../../components/formUnit/Button"

import config from "./../../../configBackend"

function InputExplorerFile(props) {
    return (
        <input type="file" name={props.name} className={props.className} onChange={props.onChange} />
    )
}

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    })
}

export class SelfieUpload extends Component {
    constructor(props) {
        super(props)
        this.state = this.initialState()
    }

    /*
    ** clickImg : if the user click on the image
    */
    initialState = () => {
        return ({
            name: null,
            imagePreviewUrl: null,
            selectedFile: null,
            selfieApiUrl : null,
            selfieApiUrlValid : false,
        })
    }

    handleName = (event) => {
        let reader = new FileReader()
        this.setState({
            selectedFile: event.target.files[0]
        })
        // if a valid file
        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result,
            })
        }
        reader.readAsDataURL(event.target.files[0])
        this.setState({ link: event.target.files[0] })
    }

    handleSubmit = (event) => {
        console.log('upload an image')
        const data = new FormData()
        event.preventDefault()

        console.log("Image upload : ")
        data.append('username', this.props.username)
        data.append('userid', this.props.userid)
        data.append('token', this.props.token)
        data.append('file', this.state.selectedFile)

        //"http://localhost:4444/img/upload"
        axios.post(config.API_IMG_UPLOAD, data, {
        })
            .then(res => {
                this.setState({
                    //imgReqApi : res.data.apiReq,
                    imagePreviewUrl : res.data.apiReq,
                })
                this.props.handleApiReqUpdate(res.data.apiReq)
                this.props.handleIdImgValueUpdate(res.data.selfieid)
                if (this.props.onSubmit)
                    this.props.onSubmit()
            })
    }

    render() {
        return (
            <div>
                <h1>welcome on my image upload</h1>
                <Image key={this.state.key} id="selfieImg" className="uploadImg" src={this.state.imagePreviewUrl} alt="fruit" />
                <InputExplorerFile name="imageUpload" className="imageUploadContainer" onChange={this.handleName} />
                <Button action={this.handleSubmit} title="upload" />
             </div>
            )
        }
    }
    
export default connect(mapStateToProps)(SelfieUpload)