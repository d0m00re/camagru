//https://node-postgres.com/features/queries#parameterized-query

import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'

import Input from './../../../components/formUnit/Input'
import NumberInput from './../../../components/formUnit/NumberInput'

import config from './../../../configBackend'

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    })
}

export class SelfieEditor extends Component {
    constructor(props) {
        super(props)

        this.state = this.initialState()
    }

    /*
    ** clickImg : if the user click on the image
    */
    initialState = () => {
        return ({
            name: null,
            selectedFile: null,
            selfieApiUrl : null,
            selfieApiUrlValid : false,

            dimOri : {h : 500, w : 500},

            idStickerSelect : -1,
            dim : {h : 100, w : 100},
            pos : {x : 0, y : 0},
            imgReqApi : null,
            idSelfie : null,
            clickImg : false,

            selfieTmpUrl : null,

            dimImgClient:{h:0, w:0},

            key : "?n=" +Date.now()
        })
    }

        // when we click on the img
    getPosClick = (event) => {
        this.setState({pos:{x : event.offsetX, y : event.offsetY}})
        const reqMerge = {'idSelfie' : this.props.idImg,
                           'idStickerSelect' : this.state.idStickerSelect,
                            'setting' :
                            {
                                'imgOri' :
                                {
                                    'dim' : this.state.dimOri
                                },
                                'imgAdd' :
                                {
                                    'dim' : this.state.dim,
                                    'pos' : {'x' : event.offsetX, 'y' : event.offsetY}
                                }
                            }
        }

        // merge img
        axios.post(config.API_POST_MERGE_IMG, reqMerge)
        .then(res => {
          //  if (res.data.error === false)
          //  {
                this.setState({key : "?n=" + Date.now()})
          //  }
        })
        .catch(err => {
        })

    }

    //http://localhost:4444/img/stickers
    // get all sticker from bdd
    componentDidMount () {
        const reqGetStickers = config.API_GET_ALL_STICKER
        const reqGetOneSticker = config.API_GET_ONE_STICKER_IMG

        // add event listener : get pos clicm on our current img
        var elem = document.getElementById("selfieImg6")
        elem.addEventListener("click", this.getPosClick)

        axios.get(reqGetStickers)
            .then(res => {
                const sticker = res.data;
                for (let i in sticker)
                    sticker[i].imgpath = reqGetOneSticker + sticker[i].imgname
                this.setState({
                    selfieApiUrl : sticker,
                    selfieApiUrlValid : true});
                console.log(sticker[0].imgpath)
            })
        this.setState({key : "?n=" + Date.now()})
    }

    /*
        update img with id of the table stickers
    */
    handleIdStickerSelect = (index, event) => {
        console.log(`Change sticker id : ${index}`)
        this.setState({idStickerSelect : index})
    }

    // update sticker width
    handleChangeDimWidthSticker = (event) => {
        let cpImgAdd =(this.state.dim)
 
        cpImgAdd.w = parseInt(event.target.value)
        this.setState({dim : cpImgAdd})
    }

    // update sticker height
    handleChangeDimHeightSticker = (event) => {
        let cpImgAdd =(this.state.dim)
 
        cpImgAdd.h = parseInt(event.target.value)
        this.setState({dim : cpImgAdd})
    }

    onImgLoad = ({target:img}) => {
        this.setState({dimImgClient:{height:img.offsetHeight,
                                   width:img.offsetWidth}});
    }

    render() {
        return (
            <div>
                <h1>Selfie editor</h1>
                <img width={this.state.dimOri.w} height={this.state.dimOri.h} id="selfieImg6" className="uploadImg"  src={this.props.apiReqDisplayImg + this.state.key} alt="selfie" />
                {this.state.selfieApiUrlValid && this.state.selfieApiUrl.map((dataSticker) =>
                        <>
                        <button type="button" onClick={this.handleIdStickerSelect.bind(this, dataSticker.stickerid)}>
                            <img src={dataSticker.imgpath} width='50' height='50'></img>
                        </button>
                        </>)
                }
                <br />

                <Input types="number" labelText="Widht" value={this.state.dim.w} onChange={this.handleChangeDimWidthSticker}/>
                <Input types="number" labelText="Height" value={this.state.dim.h} onChange={this.handleChangeDimHeightSticker}/>

                <NumberInput labelText="Width : " className="miaou" />

                <p> offstX : {this.state.pos.x} | offset y : {this.state.pos.y} | sticker : {this.state.idStickerSelect} | id selfie : {this.state.idSelfie}</p>
            </div>
            )
        }
}
    
export default connect(mapStateToProps)(SelfieEditor)