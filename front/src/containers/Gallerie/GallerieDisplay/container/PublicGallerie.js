/*
** goal: display all user selfie
** 1) fetch all selfie
** 2) map with CardSelfie.js
*/

import './PublicGallerie.css'
import './../../../../css/button.css'


import React, { Component } from 'react'
import axios from 'axios'
import CardSelfie from './../components/CardSelfie'
import StatsGallerie from '../components/StatsGallerie'

import { connect } from 'react-redux'

const config = require('./../../../../configBackend')

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    });
}

export class PublicGallerie extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selfieLink: [],
            likeState: [],
            currentPage: 0,
            maxPage: 10,

            nbcomment: 0,
            nblike: 0,
            nbselfie: 0
        }
    }

    updateLikeAPi = (index, selfieid) => {
        const tmpTab = this.state.likeState;
        const req = `${config.API_ENTRYPOINT}/selfie/action/setUnset/like/`;

        axios.post(req,
            {
                selfieid: selfieid,
            }, { headers: { 'Authorization': this.props.token } })
            .then(res => {
                tmpTab[index] = !(tmpTab[index])
                this.setState({ likeState: tmpTab })
                return (res)
            })
            .catch(err => console.log(err))
        //`http://localhost:4444/action/setUnset/like/${data.selfieid}/${this.props.userid}`
    }

    componentDidMount() {
        const reqGetAllSelfie = `${config.API_ENTRYPOINT}/selfie/get/selfies/${this.state.currentPage}`;
        const reqGetStats = `${config.API_ENTRYPOINT}/stats/gallerie/`;

        axios.get(reqGetAllSelfie)
            .then(res => {
                const gallerie = res.data
                this.setState({ selfieLink: gallerie })
                return (gallerie)
            }).then(res => {
                //modif par un promesses all pour compler les bugs
                const arrLike = new Array(res.length)
                for (let i = 0; i < res.length; i++) {
                    const reqLike = `${config.API_GET_LIKE}/${res[i].selfieid}/${this.props.userid}`
                    //perform axios request
                    axios.get(reqLike)
                        .then(res => {
                            arrLike[i] = res.data.exist
                            return arrLike
                        })
                        .then(res => {
                            this.setState({ likeState: [...arrLike] })
                        })

                }
            })
        axios.get(reqGetStats)
            .then(res => {
                const data = res.data;
                this.setState({
                    nblike: data.nblike,
                    nbcomment: data.nbcomment,
                    nbselfie: data.nbselfie
                })
                console.log('gallerie stats : ')
            })
            .catch(err => {
                console.log('error get statistics on the gallerie')
            })
    }

    nextPage = () => {
        let page = this.state.currentPage + 1
        this.setState({ currentPage: page })

        const reqGetAllSelfie = `${config.API_ENTRYPOINT}/selfie/get/selfies/${page}`

        axios.get(reqGetAllSelfie)
            .then(res => {
                const gallerie = res.data
                const merged = [...(this.state.selfieLink), ...gallerie]
                this.setState({ selfieLink: merged })
                return (gallerie)
            }).then(res => {
                //modif par un promesses all pour compler les bugs
                const arrLike = new Array(res.length)
                for (let i = 0; i < res.length; i++) {
                    const reqLike = `${config.API_GET_LIKE}/${res[i].selfieid}/${this.props.userid}`
                    //perform axios request
                    axios.get(reqLike)
                        .then(res => {
                            arrLike[i] = res.data.exist
                            return arrLike
                        })
                        .then(res => {
                            this.setState({ likeState: [...arrLike] })
                        })

                }
            })
    }

    render() {
        return (
            <div className={"gallerie"}>
                <StatsGallerie like={this.state.nblike} comment={this.state.nbcomment} nbselfie={this.state.nbselfie} username={"Gallerie"}></StatsGallerie>
                <div className={"gallerie-container"} key="gallerie-container">
                    {
                        this.state.selfieLink.map((data, index) =>
                            <React.Fragment key={'selfieCard' + index}>
                                <CardSelfie
                                    srcImg={`${config.API_ENTRYPOINT}${data.pathApiGetSelfie}`}
                                    selfieid={data.selfieid}
                                    userid={this.props.userid}
                                    stateLike={this.state.likeState[index]}
                                    likeUpdateApi={this.updateLikeAPi.bind(this, index, data.selfieid)}
                                />
                            </React.Fragment>
                        )
                    }
                </div>
                <div className="gen-center-flex" key="gen-center-flex">
                    <button className={"mybtn btn5"} onClick={this.nextPage}>Moore Selfie Please {this.state.currentPage}</button>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps)(PublicGallerie)