/*
** goal: display all user selfie
** 1) fetch all selfie
** 2) map with CardSelfie.js
*/
//select * from selfie where selfie.userid in (select myuser.userid from myuser where myuser.username like 'sar');
import './PublicGallerie.css'
import './UserGallerie.css'

import React, { Component } from 'react'
import axios from 'axios'
import CardSelfie from '../components/CardSelfie'
import StatsGallerie from '../components/StatsGallerie'

import { connect } from 'react-redux'

const config = require('../../../../configBackend')

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    })
}


export class UserGallerie extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selfieLink: [],
            likeState: [],
            username: '',
            nbcomment: '0',
            nblike: '0',
            nbselfie: '0'
        }
    }
    //this.props.username
    //`http://localhost:4444/action/setUnset/like/${data.selfieid}/${this.props.userid}`

    updateLikeAPi = (index, selfieid) => {
        const tmpTab = this.state.likeState;
        const req = `${config.API_ENTRYPOINT}/selfie/action/setUnset/like/`;

        axios.post(req,
            {
                selfieid: selfieid,
            }, { headers: { 'Authorization': this.props.token} })
            .then(res => {
                tmpTab[index] = !(tmpTab[index])
                this.setState({ likeState: tmpTab })
                return (res)
            })
            .catch(err => console.log(err))
    }

    selfieDelete = (selfieId) => {
        console.log('selfie delete : ')
        const apiRootDeleteSelfie = `${config.API_ENTRYPOINT}/selfie/delete/`
        axios.delete(apiRootDeleteSelfie,
            {
                headers: {
                    authorization: this.props.token,
                    selfieId : selfieId
                },
                data: {
                    selfieId: selfieId,
                }
            })
            .then(res => {
                console.log('success delete selfie : ' + selfieId);
            })
            .catch(err => {
                console.log('error delete selfie : ' + selfieId);
            })
    }

    // this.props.match.params
    componentDidMount() {
        //const reqGetAllSelfie = `${config.API_ENTRYPOINT}/selfie/get/apiRoute`
        this.setState({ username: this.props.match.params.username })
        const reqGetAllSelfie = `${config.API_ENTRYPOINT}/selfie/get/selfieuser/${this.props.match.params.username}`
        const reqGetUserStats = `${config.API_ENTRYPOINT}/stats/user/${this.props.match.params.username}`;

        axios.get(reqGetAllSelfie)
            .then(res => {
                const gallerie = res.data
                this.setState({ selfieLink: gallerie })
                return (gallerie)
            }).then(res => {
                //modif par un promesses all pour compler les bugs
                const arrLike = new Array(res.length)
                // recuperer les likes laisse par l utilisateur actulement connecte sur les selfie
                for (let i = 0; i < res.length; i++) {
                    // cette partie faut reecire la route pour que ca passe avec un username dirrectement
                    // const reqLike = `${config.API_GET_LIKE}/${res[i].selfieid}/${this.props.userid}`
                    const reqLike = `http://localhost:4444/selfie/get/ulikes/${res[i].selfieid}/${this.props.username}`
                    //perform axios request
                    axios.get(reqLike)
                        .then(res => {
                            arrLike[i] = res.data.exist
                            return arrLike
                        })
                        .then(res => {
                            this.setState({ likeState: [...arrLike] })
                        })
                }
            })

        axios.get(reqGetUserStats)
            .then(res => {
                this.setState({
                    nblike: res.data.nblike,
                    nbcomment: res.data.nbcomment,
                    nbselfie: res.data.nbselfie
                })

            })
            .catch(err => { console.log(err) })
    }


    render() {
        return (
            <div className={"gallerie"}>
                <StatsGallerie like={this.state.nblike} comment={this.state.nbcomment} nbselfie={this.state.nbselfie} username={this.props.match.params.username}></StatsGallerie>
                <div className={"gallerie-container"}>
                    {
                        this.state.selfieLink.map((data, index) =>
                            <React.Fragment key={`user-gallerie-${data.selfieid}`}>
                                <CardSelfie
                                    key={`${config.API_ENTRYPOINT}${data.pathApiGetSelfie}`}
                                    srcImg={`${config.API_ENTRYPOINT}${data.pathApiGetSelfie}`}
                                    selfieid={data.selfieid}
                                    userid={this.props.userid}
                                    stateLike={this.state.likeState[index]}
                                    likeUpdateApi={this.updateLikeAPi.bind(this, index, data.selfieid)}

                                    deleteOption={this.props.username == this.state.username}
                                    selfieDelete={this.selfieDelete}
                                />
                            </React.Fragment>
                        )
                    }
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps)(UserGallerie)