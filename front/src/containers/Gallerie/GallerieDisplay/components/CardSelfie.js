import React from 'react'
import Image from './../../../../components/formUnit/Image'
import PropTypes from 'prop-types';

import './Card.css'

function Like(props) {
    return (
        <>
            <button key={`like${props.selfieid}`} className={props.stateLike ? "btn-like-true" : "btn-like-false"}
                    onClick={props.selfieLike}>
            </button>
        </>
    )
}

function Comment(props) {
    return (
        <>
            <button key={`comment${props.selfieid}`} className="btn-comment" onClick={() =>
                window.location.href = `/CommentSelfie/${props.selfieid}`
            } >
            </button>
        </>
    )
}

/*
                Share {props.selfieid}
*/
function Share(props) {
    return (
        <>
            <button key={`share${props.selfieid}`} className="btn-share" onClick={props.onClick}>
            </button>
        </>
    )
}

/*
** <button className="btn-delete" onClick={null}>delete bitch {props.selfieid}</button>
*/
function Delete(props) {
    return (
        <>
            <button key={`delete${props.selfieid}`} className="btn-delete"
                    onClick={props.selfieDelete.bind(this, props.selfieid)}></button>
        </>
    )
}

function CardSelfie(props) {
    return (
        <div className="card-selfie" key={props.selfieid}>
            <Image src={props.srcImg} alt={props.alt} />
            <div className="btn-container" key={`btn-container-key-${props.selfieid}`}>
                <Like selfieid={props.selfieid} stateLike={props.stateLike} selfieLike={props.likeUpdateApi} />
                <Comment selfieid={props.selfieid} />
                <Share selfieid={props.selfieid} />
                {props.deleteOption && <Delete selfieid={props.selfieid} selfieDelete={props.selfieDelete}/>}
            </div>
        </div>
    )
}

CardSelfie.defaultProps = {
    deleteOption: false
}

export default CardSelfie
