import React from 'react'
import './StatsGallerie.css'

export default function StatsGallerie(props) {
    return (
            <div className="div-profile">
                    <section className="container-profile">
                        <h1>{props.username}</h1>
                        <div>
                            <strong> like : </strong>{props.like}
                            <strong> comment : </strong> {props.comment}
                            <strong> nb selfie : </strong> {props.nbselfie}
                        </div>
                    </section>
            </div>
    )
}
