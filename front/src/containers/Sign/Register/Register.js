import React, { Component } from 'react'
import { API_ADD_USER } from "./../../../config"
import axios from 'axios'

import md5 from 'md5'

import './Register.css'
import './../../../css/button.css'
import './../../../css/generalRules.css'

export class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            password: '',
            passwordConfirm: '',
            username: '',
            email: '',
            err: false
        };
    }

    handleUsername = (event) => {
        this.setState({ username: event.target.value })
    }

    handlePassword = (event) => {
        this.setState({ password: event.target.value })
    }

    handlePasswordConfirm = (event) => {
        this.setState({ passwordConfirm: event.target.value })
    }

    handleEmail = (event) => {
        this.setState({ email: event.target.value })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const pass = md5(this.state.password)
        console.log(`Submit : {${this.state.username}, ${this.state.password}}`)
        // generate json
        const newUser = {
            password: pass,
            username: this.state.username,
            email: this.state.email
        }

        // send data on our server
        if (this.state.passwordConfirm.localeCompare(this.state.password) === 0) {
            axios.post(API_ADD_USER, newUser)
                .then(res => {
                    this.props.history.push('/login')
                    this.clearState()
                })
                .catch(err => {
                    this.setState({ err: true, errMsg: 'This loggin/email is already taken' })
                })

        }
        else {
            this.setState({ err: true, errMsg: 'Password was not the same' })
        }
    }

    clearState = () => {
        this.setState({
            password: '',
            passwordConfirm: '',
            username: '',
            email: '',
            err: false
        })
    }

    render() {
        return (
            <>
                <fieldset className="register-fieldset text-align-center">
                    <h1>JACKSTAGRAM</h1>
                    <p className="form-desc">Signup to create and interact form your friends</p>

                    <form className="register-form">
                        <label>Username : </label>
                        <input type="text" label="username : " value={this.state.username} onChange={this.handleUsername} />
                        <label>Password : </label>
                        <input type="password" label="password : " value={this.state.password} onChange={this.handlePassword} />
                        <label>Repeat password</label>
                        <input type="password" label="repeat password : " value={this.state.passwordConfirm} onChange={this.handlePasswordConfirm} />
                        <label>Email : </label>
                        <input type="email" label="email : " value={this.state.email} onChange={this.handleEmail} />
                        <button onClick={this.handleSubmit} title="submit" className="mybtn">
                            <span>Register</span>
                        </button>

                    </form>
                    {
                        /*this.state.err && <Alert severity="error" variant="filled">{this.state.errMsg}}</Alert>*/
                    }

                    <a href="http://localhost:3000/login" variant="body2">
                        Already have an account? Sign In
                    </a>
                </fieldset>
            </>
        )
    }
}

export default Register
