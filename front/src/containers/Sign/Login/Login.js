/*
** ajouter une gesiton d erreur en cas de mauvais utilisateur
** manage login html + update redux with identificaiton information 
*/


import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'


import { connect } from 'react-redux'

import { actionStoreUserInfo, actionLogin, fetchTryConnect } from '../../../redux/redux'

import md5 from 'md5'

import './Login.css'
import './../../../css/button.css'
import './../../../css/generalRules.css'


const mapDispatchToProps = dispatch => {
    return {
        storeUserInfo: (user) => dispatch(actionStoreUserInfo({ username: user.username, userid: user.userid })),
        storeLogin : (user) => dispatch(actionLogin({payload : user})),
        storeTryLogin : (user) => dispatch(fetchTryConnect({payload : user}))
    }
}

const mapStateToProps = function (state) {
    return ({
        isConnect : state.isConnect,
        errorLoggin : state.errorLoggin,
        errorMsg : state.errorMsg
    })
}


export class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            error: false
        }
    }

    clearState = () => {
        this.setState({ username: '', password: '' })
    }

    handleUsername = (event) => {
        this.setState({ username: event.target.value })
    }

    handlePassword = (event) => {
        this.setState({ password: event.target.value })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const pass = md5(this.state.password)
        console.log(`Submit : {${this.state.username}, ${pass})}`)

        const userInfo = {
            username: this.state.username,
            password: pass,
        }

        console.log('Run Axios Request : ')
        console.log(userInfo)
        this.props.storeTryLogin(userInfo)

    }
    /*
    { this.state.error &&
    <Alert severity="error" variant="filled">Wrong Username/password</Alert> }
    */
    render() {
        return (
            <>
                <fieldset className="login-fieldset text-align-center">
                    <h1>JACKSTAGRAM</h1>
                    <p className="form-desc">Signup to create and interact form your friends</p>
                    <form className="form-login">
                        <label>Login : </label>
                        <input type="text" value={this.state.username} onChange={this.handleUsername} required/>
                        <label>Password : </label>
                        <input type="password" value={this.state.password} onChange={this.handlePassword} required/>
                        <button onClick={this.handleSubmit} title="submit" className="mybtn btn5">
                            <span>Login</span>
                        </button>
                        {this.state.error && <p className="text-error text-medium">The username or password you entered doesn't belong to an account. Please check your username/password and try again.</p>}
                    </form>

                    <a href="http://localhost:3000/register">
                        Don't have an account? Sign Up
                    </a>

                    {this.props.errorLoggin && <p>{this.props.errorMsg}</p>}
                </fieldset>
                {this.props.isConnect && <Redirect to="/selfie" />}
            </>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)