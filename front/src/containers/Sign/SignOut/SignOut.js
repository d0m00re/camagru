import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'


import { connect } from 'react-redux'
import {actionSignOut} from './../../../redux/redux'


const mapDispatchToProps = dispatch => {
    return {
        actionSignOut : () => dispatch(actionSignOut())
    }
}

class SignOut extends Component {
    componentDidMount(){
        actionSignOut();
        this.props.actionSignOut();
    }
    
    render() {
        return (
            <>
                <h1> Redirect problem, react router redirect don't work.</h1>
                <Redirect to="/login" />
            </>
        )
    }
}

export default connect(null, mapDispatchToProps)(SignOut)