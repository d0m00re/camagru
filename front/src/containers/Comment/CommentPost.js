import React, { Component } from 'react'
import axios from 'axios'
import {API_POST_COMMENT} from './../../config'

import { connect } from 'react-redux'

import './CommentPost.css'

const mapStateToProps = function (state) {
    return ({
        username : state.username,
        userid : state.userid,
        token : state.token
    });
}

export class CommentPost extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             comment : '',
             error : false,
             errorMsg : null
        }
    }

    clearState = () => {
        this.setState({comment:''});
    }
    
    handleChangeComment = (event) => {
        this.setState({comment: event.target.value})
    }

    handleSubmit = (event) =>{
        event.preventDefault()

        const commentData = {
            comment : this.state.comment,
            selfieid : this.props.selfieid,
        }

        axios.post(API_POST_COMMENT, commentData,
            {headers: {'Authorization' : this.props.token}})
        .then(res => {
            this.props.refreshMsg();
            this.clearState();
        })
        .catch(err => {
            console.log(err)
        })
    }

    render() {
        return (
            <div className="container-submit-comment">
                <form>
                    <input type="text" placeholder={"Add a comment."} onChange={this.handleChangeComment} value={this.state.comment} />
                    <button onClick={this.handleSubmit}>Post</button>
                </form>
            </div>
        )
    }
}

export default connect(mapStateToProps)(CommentPost)
