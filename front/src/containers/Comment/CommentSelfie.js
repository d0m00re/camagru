/*
** display 
*/

import React, { Component } from 'react'

import Image from './../../components/formUnit/Image'

import CommentView from './CommentView'
import CommentPost from './CommentPost'

import axios from 'axios'

import './CommentSelfie.css'

export class CommentSelfie extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selfieid: -1,

            dataComment: [],

            error: false,
            errorMsg: null
        }
    }

    updateMessage = () => {
        /*
        console.log('update comment locay : ' + msg)
        let commentTab = this.state.dataComment
        console.log(this.state.commentTab)
        commentTab.push({ comment: msg })
        this.setState({ dataComment: commentTab })
        */

        const selfieid = parseInt(this.props.match.params.selfieid)
        this.setState({ selfieid: selfieid })
        //get the message
        const reqGetComment = `http://localhost:4444/comment/get/selfie/${selfieid}`

        axios.get(reqGetComment)
            .then(res => {
                this.setState({ dataComment: res.data })
            })
            .catch(err => {
                this.setState = {error : true, errorMsg : "can't get comment list"}
            })
    }

    componentDidMount() {
        this.updateMessage()
    }

    render() {
        return (
            <section className="comment-selfie-container">
                    <div className="selfie-container">
                        <Image className={"img-comment"} src={`http://localhost:4444/img/get/selfie/${this.state.selfieid}`} />
                    </div>
                    <div className={"comment-container"}>
                        <CommentView selfieid={this.state.selfieid} comment={this.state.dataComment} />
                        <CommentPost selfieid={this.state.selfieid} refreshMsg={this.updateMessage} />
                    </div>
            </section>
        )
    }
}

export default CommentSelfie
