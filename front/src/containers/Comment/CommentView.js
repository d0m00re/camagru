import React, { Component } from 'react'
import axios from 'axios'
import { API_GET_USER_WT_ID } from './../../config'

import { connect } from 'react-redux'

import './CommentView.css'

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    })
}

class Comment extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: 'unknown'
        }
    }

    componentDidMount() {
        axios.get(API_GET_USER_WT_ID + this.props.userid,  { retry: 5, retryDelay: 1000 })
            .then((resp) => {
                this.setState({ username: resp.data[0].username})
            })
            .catch((err) => {
                this.setState({username : 'anonymous'})
            }) 
            
        }


    render() {
        return (
            <div key={this.props.commentid}>
                    {this.state.username} : {this.props.comment}
            </div>
        )
    }
}

class CommentView extends Component {
    render() {
        if (parseInt(this.props.selfieid) === -1 || this.props.comment.length === 0) {
            return (
                <>
                </>
            )
        }
        else {
            return (
                <div className="comment-container">
                    {
                        this.props.comment.map((data, index) =>
                            <React.Fragment key={data.comment + index}>
                                <Comment userid={data.userid} comment={data.comment} />
                            </React.Fragment>
                        )
                    }
                </div>
            )
        }
    }
}

export default connect(mapStateToProps)(CommentView)