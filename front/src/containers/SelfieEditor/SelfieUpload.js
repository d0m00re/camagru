//https://node-postgres.com/features/queries#parameterized-query
import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'

import config from "./../../configBackend"

import './SelfieUpload.css'
import '../../css/button.css'

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    })
}

export class SelfieUpload extends Component {
    constructor(props) {
        super(props)
        this.state = this.initialState();
    }

    initialState = () => {
        return ({
            name: null,
            imagePreviewUrl: null,
            selectedFile: null,
            selfieApiUrl: null,
            selfieApiUrlValid: false,
        })
    }

    handleName = (event) => {
        let reader = new FileReader()
        this.setState({
            selectedFile: event.target.files[0]
        })
        // if a valid file
        reader.onloadend = () => {
            this.setState({
                imagePreviewUrl: reader.result,
            })
        }
        reader.readAsDataURL(event.target.files[0])
        this.setState({ link: event.target.files[0] })
        console.log('update file : ')
        console.log(event.target.files[0])
    }

    /*
    ** upload selfie
    */
    handleSubmit = (event) => {
        const data = new FormData()
        event.preventDefault()

        console.log("Image upload : ")
        data.append('file', this.state.selectedFile)

        axios.post(config.API_IMG_UPLOAD,
            data,
            { headers: { 'Authorization': this.props.token } })
            .then(res => {
                this.setState({
                    imagePreviewUrl: res.data.apiReq,
                })
                this.props.handleApiReqUpdate(res.data.apiReq)
                this.props.handleIdImgValueUpdate(res.data.selfieid)
                if (this.props.onSubmit)
                    this.props.onSubmit()
            })
    }

    render() {
        return (
            <div className="container-upload-img">
                {(this.state.imagePreviewUrl == null) ? <h1>Import your Image</h1> : <h1>Save your image</h1>}
                <img key={this.state.key} id="selfieImg" className="uploadImg" src={this.state.imagePreviewUrl} alt="my-selfie-img"/>
                {this.state.imagePreviewUrl == null &&
                    <><label htmlFor="file" className="label-file">Choose a picture</label>
                        <input className="input-file" id="file" type="file" name="imageUpload" onChange={this.handleName} /></>}
                {this.state.imagePreviewUrl && <button className={"mybtn btn5"} onClick={this.handleSubmit}><span>Save in your collection</span></button>}
            </div>
        )
    }
}

export default connect(mapStateToProps)(SelfieUpload)