//https://node-postgres.com/features/queries#parameterized-query

import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import config from './../../configBackend'
import { Redirect } from 'react-router-dom'

import './SelfieEditorV2.css'
import './SelfieRules.css'
import '../../css/button.css'

const mapStateToProps = function (state) {
    return ({
        username: state.username,
        userid: state.userid,
        token: state.token
    })
}

export class SelfieEditorV2 extends Component {
    constructor(props) {
        super(props)

        this.state = this.initialState()
    }

    initialState = () => {
        return ({
            selectedFile: null,
            selfieApiUrl: null,
            selfieApiUrlValid: false,

            dimOri: { h: 500, w: 500 },

            imgReqApi: null,
            idSelfie: null,

            selfieTmpUrl: null,

            dimImgClient: { h: 0, w: 0 },

            idStickerSelect: 1,
            posClick: { x: 0, y: 0 },
            dimSticker: { h: 64, w: 64 },

            key: "?n=" + Date.now(),

            redirect : false,
            redirectLink : '/gallerie'
        })
    }

    // when we click on the img
    getPosClick = (event) => {
        const canvas = this.refs.canvas
        const ctx = canvas.getContext("2d")

        this.setState({ posClick: { x: event.offsetX, y: event.offsetY } })
        console.log({ posClick: { x: event.offsetX, y: event.offsetY } })

        const stickerName = 'sticker' + this.state.idStickerSelect
        const sticker = document.getElementsByClassName(stickerName)[0]
        ctx.drawImage(sticker, event.offsetX - this.state.dimSticker.w / 2, event.offsetY - this.state.dimSticker.h / 2, this.state.dimSticker.w, this.state.dimSticker.h)
    }

    componentDidMount() {
        const canvas = this.refs.canvas
        const ctx = canvas.getContext("2d")

        // image loadding
        const img = new Image()
        img.onload = function () {
            ctx.drawImage(img, 0, 0, 640, 640)
        }

        img.setAttribute('crossOrigin', 'anonymous');
        img.src = this.props.apiReqDisplayImg;
        //-------------------------------------------
        // attach the event listenener
        var elemCanva = document.getElementById("image")
        elemCanva.addEventListener("click", this.getPosClick)

        //-------------------------------------------------
        const reqGetStickers = config.API_GET_ALL_STICKER
        const reqGetOneSticker = config.API_GET_ONE_STICKER_IMG

        console.log(reqGetStickers)
        console.log(reqGetOneSticker)

        axios.get(reqGetStickers)
            .then(res => {
                const sticker = res.data;
                for (let i in sticker)
                    sticker[i].imgpath = reqGetOneSticker + sticker[i].imgname
                this.setState({
                    selfieApiUrl: sticker,
                    selfieApiUrlValid: true
                });
                console.log(sticker[0].imgpath)
            })
            .catch(err => {
                console.log(err)
            })
    }

    /*
        update img with id of the table stickers
    */
    handleIdStickerSelect = (index, event) => {
        console.log(`Change sticker id : ${index}`)
        this.setState({ idStickerSelect: index })
    }

    // update sticker width
    handleChangeDimWidthSticker = (event) => {
        let cpImgAdd = (this.state.dimSticker)

        cpImgAdd.w = parseInt(event.target.value)
        this.setState({ dimSticker: cpImgAdd })
    }

    // update sticker height
    handleChangeDimHeightSticker = (event) => {
        let cpImgAdd = (this.state.dimSticker)

        cpImgAdd.h = parseInt(event.target.value)
        this.setState({ dimSticker: cpImgAdd })
    }

    handleUpdate = (event) => {
        console.log('selfieEditorV2 : update------')
        const self = this;
        const data = new FormData()

        data.append('username', this.props.username)
        data.append('userid', this.props.userid)
        data.append('token', this.props.token)
        data.append('idselfie', this.props.idImg)

        const canvas = document.getElementById('image')
        canvas.toBlob(function (blop) {
            console.log(self.props.token);
            console.log(self.props.idImg)
            data.append('file', blop, 'fuck.png') 
            console.log('putain')
            axios.post(config.API_IMG_UPDATE, 
                        data,
                        {headers: {'Authorization' : self.props.token, 'selfieid' : self.props.idImg}
                        })
                .then(res => {
                    console.log('redirection motherfucker on gallerie')
                    self.setState({redirect : true});
                })
                .catch(err => {
                    console.log(`selfieEditorV2 : error --> ${err}`)
                })
        })
    }

    /*
    ** retrieve the original image
    ** reload the props image
    */
    handleReset = (event) => {
        const canvas = this.refs.canvas
        const ctx = canvas.getContext("2d")

        // image loadding
        const img = new Image()

        img.onload = function () {
            ctx.drawImage(img, 0, 0, 640, 640)
        }
        img.src = this.props.apiReqDisplayImg
    }

    onImgLoad = ({ target: img }) => {
        this.setState({
            dimImgClient: {
                height: img.offsetHeight,
                width: img.offsetWidth
            }
        });
    }

    render() {
        return (
            <div className="container-selfie">
                <section className="selfie-button-action">
                        <button className="mybtn btn5" onClick={this.handleUpdate}>Save your image</button>
                        <button className="mybtn btn5" onClick={this.handleReset}>Restore initial image</button>
                </section>

                <canvas ref="canvas" width={640} height={640} id="image"></canvas>

                <div className="container-editor-elem">
                    <section className="container-sticker">
                        <h1>STICKER</h1>
                        <div className="stickerContainer">
                            {this.state.selfieApiUrlValid && this.state.selfieApiUrl.map((dataSticker) =>
                                <React.Fragment key={`SelfieEditor${dataSticker.stickerid}`}>
                                    <button type="button" onClick={this.handleIdStickerSelect.bind(this, dataSticker.stickerid)}>
                                        <img alt={dataSticker.imgname} crossOrigin="Anonymous" src={dataSticker.imgpath} width='50' height='50' className={`sticker sticker${dataSticker.stickerid}`}></img>
                                    </button>
                                </React.Fragment>)
                            }
                        </div>
                    </section>
                    <section>
                        <h1>DIM STICKER</h1>
                        <input type="number" label="Widht" value={this.state.dimSticker.w} onChange={this.handleChangeDimWidthSticker} />
                        <input type="number" label="Height" value={this.state.dimSticker.h} onChange={this.handleChangeDimHeightSticker} />
                    </section>
                </div>

                {this.state.redirect && <Redirect to={this.state.redirectLink} />}
            </div>
        )
    }
}

export default connect(mapStateToProps)(SelfieEditorV2)