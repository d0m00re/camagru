import React, { Component } from 'react'

import SelfieUpload from './SelfieUpload'
import SelfieEditorV2 from './SelfieEditorV2'

/*
** state = 0 : need to upload a picutre
** state = 1 : selfie edition

idImg : id of he selfie in my db
*/
export class Selfie extends Component {
    constructor(props) {
        super(props)

        this.state = {
            stateUpload: 0,
            stateLoad: 0,
            apiReqDisplayImg: null,
            idImg: -1
        }
    }

    changeApiReqUpdate = (apiUrl) => {
        this.setState({ apiReqDisplayImg: apiUrl })
    }

    changeUploadStateToEdition = () => {
        this.setState({ stateUpload: 1 })
    }

    changeUploadStateToUpload = () => {
        this.setState({ stateUpload: 0 })
    }

    changeLoadStateTrue = () => {
        this.setState({ stateLoad : 1})
    }

    changeLoadStateFalse = () => {
        this.setState({ stateLoad : 0})
    }

    changeIdImgValue = (id) => {
        this.setState({ idImg: id })
    }

    render() {
        console.log('State : ')
        console.log(this.state)
        return (
            <div className="selfieCreator">
                {
                    this.state.stateUpload === 0 &&
                    <SelfieUpload onSubmit={this.changeUploadStateToEdition}
                        handleApiReqUpdate={this.changeApiReqUpdate}
                        handleIdImgValueUpdate={this.changeIdImgValue} />
                }
                {
                    this.state.stateUpload === 1 &&
                    <SelfieEditorV2 onSubmit={this.changeUploadStateToUpload}
                        apiReqDisplayImg={this.state.apiReqDisplayImg}
                        idImg={this.state.idImg} />
                }
            </div>
        )
    }
}

export default Selfie
