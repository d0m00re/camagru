import './Navbar.css';
import './App.css'

import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import Login from './containers/Sign/Login/Login'
import Register from './containers/Sign/Register/Register'
import SignOut from './containers/Sign/SignOut/SignOut'

import { connect } from 'react-redux';

import PublicGallerie from './containers/Gallerie/GallerieDisplay/container/PublicGallerie';
import UserGallerie from './containers/Gallerie/GallerieDisplay/container/UserGalerie'
import CommentSelfie from './containers/Comment/CommentSelfie'
import EditAccount from './containers/Sign/EditAccount/EditAccount'
import { actionSignOut } from './redux/redux'

import Selfie from './containers/SelfieEditor/Selfie'

import { SvgRegister, SvgLogin, SvgGallerie, SvgProfile, SvgSelfieCreator, SvgLogOut } from './components/Svg/SvgImg'

import SelfieEditorV2 from './containers/SelfieEditor/SelfieEditorV2';

const mapDispatchToProps = dispatch => {
  return {
    actionSignOut: () => dispatch(actionSignOut())
  }
}

const mapStateToProps = function (state) {
  return {
    username: state.username,
    isConnect: state.isConnect,
    userid : state.userid,
    token :state.token
  }
}

function Debug(props) {
  return (
    <>
      <p>username : {props.username} | isCOnnect : {props.isConnect} | userid : {props.userid}</p>
    </>
  )
}

function SvgCreateLink(props) {
  const Svg = props.Svg;


  let className = "nav-item";

  if (props.className)
    className = "nav-item " + props.className;
  return (
    <>
      {props.show &&
        <li className={className}>
          <a href={props.href} className="nav-link">
            <Svg />
            <span className="link-text">{props.label}</span>
          </a>
        </li>}
    </>
  )
}

function MainLinkSub(props) {
  return (
    <>
      <nav className="navbar">
        <ul className="navbar-nav">

          <SvgCreateLink show={!(props.isConnect)} label="Register" href={"/register"} Svg={SvgRegister} />

          <SvgCreateLink show={!(props.isConnect)} label="Login" href={"/login"} Svg={SvgLogin} />

          <SvgCreateLink show={true} label="Gallerie" href={"/gallerie"} Svg={SvgGallerie} />

          <SvgCreateLink show={(props.isConnect)} label="Profile" href={`/galleries/${props.username}`} Svg={SvgProfile} />

          <SvgCreateLink show={(props.isConnect)} label="Selfie Creator" href={"/selfie"} Svg={SvgSelfieCreator} />

          <SvgCreateLink className={"nav-item last-elem"} show={props.isConnect} label="Log out" href={'/signOut'} Svg={SvgLogOut} />
        </ul>
      </nav>
    </>
  )
}

function MainRoot(props) {
  return (
    <>
      <Switch>
        <Route exact path="/" component={PublicGallerie} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/editAccount" component={EditAccount} />
        <Route exact path="/gallerie" component={PublicGallerie} />
        <Route exact path="/galleries/:username" component={UserGallerie} />
        <Route exact path="/selfie" component={Selfie} />
        <Route path="/CommentSelfie/:selfieid" component={CommentSelfie} />
        <Route path="/SelfieEditor" component={() => <SelfieEditorV2 apiReqDisplayImg={"http://localhost:4444/img/selfie/get/jackouille/1592228140677-cat3.png"}/>}/>
        <Route path="/User/:userid" component={Error404} />
        <Route exact path="/signOut" component={SignOut} />
        <Route component={Error404} />
      </Switch>
    </>
  )
}

function Error404(props) {
  return (
    <>
      <h1>Error 404</h1>
    </>
  )
}

function App(props) {
  console.log(props)
  return (
    <div className="App">
      <Router>
        <MainLinkSub isConnect={props.isConnect} username={props.username} />
        <MainRoot isConnect={props.isConnect} username={props.username} />
      </Router>
      {/*<Debug username={props.username} isConnect={props.isConnect} userid={props.userid} token={props.token}/>*/}
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
