import React from 'react'
import TextField from '@material-ui/core/TextField';

/*
<label>{props.labelText}</label>
            <input type={props.types}
                   value={props.value}
                   onChange={props.onChange}
                   placeholder={props.placeholder}>
            </input>
*/

export default function Input(props) {
    return (
        <div className="inputClass">
            <TextField 
                id="standard-basic"
                label={props.labelText}
                type={props.types}
                value={props.value}
                onChange={props.onChange}
                placeholder={props.placeholder}
                required
                fullWidth
                
                variant="outlined"
                margin="normal"
                />
        </div>
    )
}