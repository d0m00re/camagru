import React from 'react';
import {withRouter} from 'react-router-dom';
/*
<button onClick={props.action}>
            {props.title}
        </button>
*/
export default function Button(props) {
    return (
        <>
        <button onClick={props.action}>{props.title}</button>
        </>
    )
}
