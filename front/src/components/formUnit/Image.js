import React from 'react'

/*
** className, src, alt
*/
export default function Image(props) {
    return (
        <>
            <img className={props.className}
                 src={props.src}
                 alt={props.alt}
                 id={props.id}>
            </img>
        </>
    )
} 