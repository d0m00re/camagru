import React, { Component } from 'react'

export class NumberInput extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             numberW : 2
        }
    }

    handleNumberW = (event) => {
        this.setState({numberW : event.target.value})
    }
    

    render() {
        console.log(this.state.numberW)
        return (
            <div>
                <label>{this.props.labelText}</label>
                <input type='number'
                        value={this.state.numberW}
                        onChange={this.handleNumberW}/>
            </div>
        )
    }
}

export default NumberInput
