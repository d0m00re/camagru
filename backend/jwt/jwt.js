const pool = require('./../db/connect')
const jwt = require('jsonwebtoken')

const privateKey = '42secretmtf'; // a externaliser

export function jwtSignGenerate (username) {
  let jsonOriginalToken = {username : username.username, userid : username.userid, role : 'user'}
  const token = jwt.sign(jsonOriginalToken, privateKey);//, { algorithm: 'RS256'});

  return (token);
}

/*
** decode token and store it in req.tokenDecoded
*/
export function verifyToken (req, res, next) {  
  console.log(req.headers.authorization)
  const bearerHeader = req.headers.authorization

  if (typeof bearerHeader !== 'undefined') {
    const bearerToken = bearerHeader
    req.token = bearerToken

    let decoded = jwt.verify(req.token, privateKey);//, {algorithms: ['RS256']})
    req.tokenDecoded = decoded;
    console.log('decoded original token :')
    console.log(decoded)
    next()
  } else {
    res.sendStatus(403)
  }
}

/*
** decode token and verif selfie owning
*/
export function verifyTokenSelfieOwn (req, res, next) {
  /*
** check user authorisation
*/
  const sqlCheckAuthorisation = "SELECT userid FROM selfie WHERE selfieid=$1 AND userid=$2;";
  const bearerHeader = req.headers.authorization

  if (typeof bearerHeader !== 'undefined') {
    const tokenDecoded = jwt.verify(bearerHeader, privateKey);
    req.tokenDecoded = tokenDecoded;
    const selfieId = req.headers.selfieid;
    const userId = tokenDecoded.userid;

    const queryCheckUserAuth = {
      name: 'sql-check-auth',
      text: sqlCheckAuthorisation,
      values: [selfieId, userId]
    }

    pool.query(queryCheckUserAuth)
    .then(success => {
      if (success.rowCount == 0)
        throw 'error - selfie not own by the user';
      console.log('User own selfie.')
      next();
    })
    .catch(err => {
      console.log(err);
      res.sendStatus(403);
    })
  }
}
