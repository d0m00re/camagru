/*
** init data
*/

import { PATH_STICKER_IMG } from './toolManagePicture/setting'

const sticker = require('./db/models/sticker')
const pool = require('./db/connect')
const queryPerform = require('./db/utils/queryPerform')

function update(elem){
  //  console.log(elem)
}

function initStickerData()
{
        const data = [
            {'StickerAuth' : -1, 'ImgPath' : PATH_STICKER_IMG + 'christmas.png', 'ImgName' : 'christmas.png'},
            {'StickerAuth' : -1, 'ImgPath' : PATH_STICKER_IMG + 'poussin.png', 'ImgName' : 'poussin.png'},
            {'StickerAuth' : -1, 'ImgPath' : PATH_STICKER_IMG + 'rotten-tomatoes.png', 'ImgName' : 'rotten-tomatoes.png'},
            {'StickerAuth' : -1, 'ImgPath' : PATH_STICKER_IMG + 'simplybuilt.png', 'ImgName' : 'simplybuilt.png'},
            {'StickerAuth' : -1, 'ImgPath' : PATH_STICKER_IMG + 'snake.png', 'ImgName' : 'snake.png'},
            {'StickerAuth' : -1, 'ImgPath' : PATH_STICKER_IMG + 'terrarium.png', 'ImgName' : 'terrarium.png'}
        ]

        //queryPerform(pool, sticker.DELETE_ALL_RECORD_STICKER_IMG, update)

        console.log("--== SAVE STICKER IN DATABASE ==--")
        for (let i in data)
        {
            console.log(data[i])
            queryPerform(pool, sticker.forgeSqlStickerAdd(data[i]) , update)
        }
      //  queryPerform(pool, sticker.forgeSqlStickerAdd(data[0]) , update)

      //  queryPerform(pool, sticker.forgeSqlStickerAdd(data[1]) , update)
}

initStickerData()