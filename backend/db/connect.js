const pg = require('pg')

//import dbInfo from('./db/config')
const {dbInfo} = require("./config")

const pool = new pg.Pool(dbInfo)

pool.connect((err, client, release) => {
  if (err) {
    return console.error('Error acquiring client', err.stack)
  }
  client.query('SELECT NOW()', (err, result) => {
    release()
    if (err) {
      return console.error('Error executing query', err.stack)
    }
    console.log(result.rows)
  })
})

module.exports = pool;