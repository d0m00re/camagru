const Selfie = require('../models/selfie')
const Like = require('../models/like')
const Sticker = require('../models/sticker')
const User = require('../models/user')
const Comment = require('../models/comment')

const PATH_STICKER_IMG = require('../../toolManagePicture/setting').PATH_STICKER_IMG

const generateDbCreateTab = [
    User.CREATE_USER_TABLE,
    Sticker.CREATE_STICKER_IMG_TABLE,
    Selfie.CREATE_SELFIE_TABLE,
    Like.CREATE_LIKE_TABLE,
    Comment.CREATE_COMMENT_TABLE
];

const generateDbDropTab = [
    Like.DROP_LIKE_TABLE,
    Comment.DROP_COMMENT_TABLE,
    Sticker.DROP_TABLE_STICKER_IMG,
    Selfie.DROP_SELFIE_TABLE,
    User.DROP_USER_TABLE,
];

const forgeSqlStickerAdd = Sticker.forgeSqlStickerAdd;

const generateDbStickerTab = [
    { 'StickerAuth': -1, 'ImgPath': PATH_STICKER_IMG + 'christmas.png', 'ImgName': 'christmas.png' },
    { 'StickerAuth': -1, 'ImgPath': PATH_STICKER_IMG + 'poussin.png', 'ImgName': 'poussin.png' },
    { 'StickerAuth': -1, 'ImgPath': PATH_STICKER_IMG + 'rotten-tomatoes.png', 'ImgName': 'rotten-tomatoes.png' },
    { 'StickerAuth': -1, 'ImgPath': PATH_STICKER_IMG + 'simplybuilt.png', 'ImgName': 'simplybuilt.png' },
    { 'StickerAuth': -1, 'ImgPath': PATH_STICKER_IMG + 'snake.png', 'ImgName': 'snake.png' },
    { 'StickerAuth': -1, 'ImgPath': PATH_STICKER_IMG + 'terrarium.png', 'ImgName': 'terrarium.png' }
]

for (let i in generateDbStickerTab) {
    generateDbStickerTab[i] = forgeSqlStickerAdd(generateDbStickerTab[i])
}

module.exports = {
    generateDbCreateTab,
    generateDbDropTab,
    generateDbStickerTab
}