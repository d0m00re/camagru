/*
** extract path end for construct the api root
*/

export function extractEndPath(path) {
    const pathExtract = path.split('/')
    const len = pathExtract.length

    if (len < 2)
        return (null)

    return (pathExtract[len - 2] + '/' + pathExtract[len - 1])
}

