function queryPerform(client, request, fUpdate){
    client
    .query(request)
    .then(result => fUpdate(result))
    .catch(e => fUpdate(e))
  }

  function queryPerformErr(client, request, fUpdate, fError){
    client
    .query(request)
    .then(result => fUpdate(result))
    .catch(e => fError(e))
  }

  module.exports = {queryPerform, queryPerformErr} 