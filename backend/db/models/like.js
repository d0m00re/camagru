const NAME_LIKE_TABLE = 'LIKESF'

const CREATE_LIKE_TABLE = `CREATE TABLE IF NOT EXISTS ${NAME_LIKE_TABLE} (\
likeid SERIAL PRIMARY KEY,\
userid INTEGER,\
selfieid INTEGER,\
FOREIGN KEY (userid) REFERENCES myuser(userid),\
FOREIGN KEY (selfieid) REFERENCES Selfie(selfieid),\
CONSTRAINT UC_LIKE UNIQUE (userid, selfieid));`

const DROP_LIKE_TABLE = `DROP TABLE IF EXISTS ${NAME_LIKE_TABLE} CASCADE;`

const SELECT_ALL = `SELECT * FROM ${NAME_LIKE_TABLE};`

const SELECT_ONE_LIKE = `SELECT * FROM ${NAME_LIKE_TABLE}\
                        WHERE ${NAME_LIKE_TABLE}.selfieid = $1 \
                        AND ${NAME_LIKE_TABLE}.userid = $2;`

const SELECT_ONE_LIKE_WT_USERNAME = `SELECT * FROM ${NAME_LIKE_TABLE}
                                    WHERE ${NAME_LIKE_TABLE}.selfieid = $1
                                    AND ${NAME_LIKE_TABLE}.userid \
                                    IN (select myuser.userid from myuser where myuser.username like '$2');`

const INSERT_ONE_LIKE = `INSERT INTO ${NAME_LIKE_TABLE} (selfieid, userid)\
VALUES ($1, $2);`

const DELETE_ONE_LIKE = `DELETE FROM ${NAME_LIKE_TABLE} \
                            WHERE ${NAME_LIKE_TABLE}.selfieid = $1 \
                            AND ${NAME_LIKE_TABLE}.userid = $2;`

function forgeSqlAddLike(data){
    const userId = data.userid
    const selfieid = data.selfieid

    const req = `INSERT INTO ${NAME_LIKE_TABLE} (userId, selfieid)\
                VALUES (${userId}, ${selfieid});`
    return (req)
}

module.exports = {
    NAME_LIKE_TABLE,
    CREATE_LIKE_TABLE,
    DROP_LIKE_TABLE,
    SELECT_ALL,
    SELECT_ONE_LIKE,
    SELECT_ONE_LIKE_WT_USERNAME,
    INSERT_ONE_LIKE,
    DELETE_ONE_LIKE,
    forgeSqlAddLike
}