const NAME_SELFIE_TABLE = 'Selfie'

const CREATE_SELFIE_TABLE = `CREATE TABLE IF NOT EXISTS ${NAME_SELFIE_TABLE} (\
selfieid SERIAL PRIMARY KEY,\
userId INTEGER,\
imgPath varchar(150),\
imgName varchar(50),\
FOREIGN KEY (userId) REFERENCES myuser(userId));`

const SELECT_ALL_SELFIE_TABLE = `SELECT * from ${NAME_SELFIE_TABLE};`

const SELECT_USER_SELFIE_TABLE =  `SELECT * from selfie where selfie.userid in (select myuser.userid from myuser where myuser.username like '$1');`;

const SELECT_ONE_SELFIE_WT_ID = `SELECT * FROM ${NAME_SELFIE_TABLE}\
 WHERE ${NAME_SELFIE_TABLE}.selfieid = $1;`

const DROP_SELFIE_TABLE = `DROP TABLE IF EXISTS ${NAME_SELFIE_TABLE} CASCADE;`

/*
** userId
** imgPath
** imgName
*/
function forgeSqlAddPaquet(data)
{
    const userId = data.userId
    const imgPath = data.imgPath
    const imgName = data.imgName

    if (!userId || !imgPath || !imgName)
        return (null)
    
    const req = `INSERT INTO ${NAME_SELFIE_TABLE} (userId, imgPath, imgName)\
                VALUES (${userId}, '${imgPath}', '${imgName}') RETURNING selfieid;`
    return (req)
}

module.exports = {NAME_SELFIE_TABLE,
                CREATE_SELFIE_TABLE,
                SELECT_ALL_SELFIE_TABLE,
                SELECT_USER_SELFIE_TABLE,
                DROP_SELFIE_TABLE,
                SELECT_ONE_SELFIE_WT_ID,
                forgeSqlAddPaquet}
