const NAME_COMMENT_TABLE = 'COMMENTSF'

const CREATE_COMMENT_TABLE = `CREATE TABLE IF NOT EXISTS ${NAME_COMMENT_TABLE} (\
                              commentid SERIAL PRIMARY KEY,\
                              userId INTEGER,\
                              selfieid INTEGER,\
                              FOREIGN KEY (userid) REFERENCES myuser(userid),\
                              FOREIGN KEY (selfieid) REFERENCES Selfie(selfieid),\
                              comment VARCHAR(250) NOT NULL,\
                              date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP);`

const DROP_COMMENT_TABLE = `DROP TABLE IF EXISTS ${NAME_COMMENT_TABLE} CASCADE;`

const SELECT_ALL = `SELECT * FROM ${NAME_COMMENT_TABLE};`

const CREATE_COMMENT_INSER_INTO = `INSERT INTO ${NAME_COMMENT_TABLE} (userId, selfieid, comment)\
                                    VALUES ($1, $2, $3);`

const ADD_COMMENT = `INSERT INTO ${NAME_COMMENT_TABLE} (userid, selfieid, comment)\
            VALUES ($1, $2, $3);`

const GET_SELFIE_COMMENTS = `SELECT * FROM ${NAME_COMMENT_TABLE} WHERE ${NAME_COMMENT_TABLE}.selfieid = $1;`

function forgeSqlComment(data){
    const userid = data.userId
    const selfieid = data.selfieid
    const comment = data.comment

    const req = `INSERT INTO ${NAME_COMMENT_TABLE} (userId, selfieid, comment)\
                 VALUES (${userid}, ${selfieid}, '${comment}');`
    return (req)
}

module.exports = {
    NAME_COMMENT_TABLE,
    CREATE_COMMENT_TABLE,
    SELECT_ALL,
    DROP_COMMENT_TABLE,
    CREATE_COMMENT_INSER_INTO,
    GET_SELFIE_COMMENTS,
    ADD_COMMENT,
    forgeSqlComment
}