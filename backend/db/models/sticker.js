

/*
** this table is use for storing stickers, for the user utilisation
** authorisation : futur personalised stickers for group of users
** imgPath : path for the stickers
** imgName : path for the img name
*/

const NAME_TABLE = 'Sticker'

const CREATE_STICKER_IMG_TABLE = `CREATE TABLE IF NOT EXISTS ${NAME_TABLE} (\
    StickerId SERIAL PRIMARY KEY,\
    StickerAuth integer,\
    ImgPath varchar(150),\
    ImgName varchar(50));`

const DROP_TABLE_STICKER_IMG = `DROP TABLE IF EXISTS ${NAME_TABLE} CASCADE;`

const SELECT_ALL_STICKER_IMG = `SELECT * FROM ${NAME_TABLE};`

const DELETE_ALL_RECORD_STICKER_IMG =`TRUNCATE TABLE ${NAME_TABLE};`// `DELETE * FROM ${NAME_TABLE};`

function forgeSqlStickerAdd(data){
    const StickerAuth = data.StickerAuth
    const ImgPath = data.ImgPath
    const ImgName = data.ImgName

    if (!ImgPath || !ImgName)
        return (null)
    
    const req = `INSERT INTO ${NAME_TABLE} (StickerAuth, ImgPath, ImgName)\
                VALUES (${StickerAuth}, '${ImgPath}', '${ImgName}');`
    return (req)
}

module.exports = {
    CREATE_STICKER_IMG_TABLE,
    DROP_TABLE_STICKER_IMG,
    SELECT_ALL_STICKER_IMG,
    DELETE_ALL_RECORD_STICKER_IMG,
    forgeSqlStickerAdd
}