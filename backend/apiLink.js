export const port = 4444
export const serverName = 'localhost'
export const server = 'http://' + serverName + `:${port}`

/*
** generate api request
*/

export const get_selfie_img = '/img/selfie/get/'
export const api_get_selfie_img = `${get_selfie_img}:username/:image` // :username/:image

export function gen_api_get_selfie_img(username, imageName){
    return (get_selfie_img + username + '/' + imageName)
}

//----------------------