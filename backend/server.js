const apiLink = require('./apiLink')

const express = require('express')
const { queryPerform, queryPerformErr } = require('./db/utils/queryPerform')
const cors = require('cors')
const app = express()
const port = process.env.PORT || apiLink.port
const fs = require('fs')
const { SELECT_ALL_STICKER_IMG } = require('./db/models/sticker')


//gestion de la structure pour save les images
const filesystem = require('./toolManagePicture/utils')

// route use
const userRouter = require('./route/user')
const selfieRouter = require('./route/selfie')
const imgRouter = require('./route/img')
const commentRouter = require('./route/comment')
const statsRouter = require('./route/stats')

app.use('/user', userRouter)
app.use('/selfie', selfieRouter)
app.use('/img', imgRouter)
app.use('/comment', commentRouter)
app.use('/stats', statsRouter)

//----------------------------------------------
// connect with db
const pool = require('./db/connect')

app.use(express.json())
app.use(cors())
app.all('*', cors())

// hoc function
function updateJSONWithRes(res) {
  return function (elem) {
    res.json(elem.rows)
  }
}

app.get('/', function (req, res, next) {
  res.json("<h1> BACKEND API </h1>")
})

app.get('/profile/picc', function (req, res) {
  res.sendFile(`./img/profile/cat.png`, { root: __dirname })
})

const defaultLocationImg = `./img/profile/cat.png`

app.get('/profile/pic/:username', function (req, res) {
  const username = req.params.username
  const pathImg = `./img/profile/${username}.png`

  try {
    if (fs.existsSync(pathImg)) {
      res.sendFile(pathImg, { root: __dirname })
    }
    else
      res.sendFile(defaultLocationImg, { root: __dirname })
  } catch (err) {
    res.sendFile(defaultLocationImg, { root: __dirname })
  }
})

app.get('/sticker/all', function (req, res) {
  const sqlReq = SELECT_ALL_STICKER_IMG

  queryPerform(pool, sqlReq, updateJSONWithRes(res))
})

app.listen(port, function () {
  filesystem.createFilesSystemImg()
  console.log(`server running on port ${port}`)
})
