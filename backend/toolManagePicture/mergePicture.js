const fs = require('fs')
const { createCanvas, loadImage } = require('canvas')


/*
merge two img between them
---- argument
img1Path : path + name first image
img2Path : path + name second image
pathNameOutput : where we store the new img
setting : general setting on imge
*/

export function merge2Img(img1Path, img2Path, pathNameOutput, setting, res)
{
   // const img1 = new Canvas(200, 200)
   const canvas = createCanvas(setting.imgOri.dim.w, setting.imgOri.dim.h)
   const ctx = canvas.getContext('2d', {alpha: true, pixelFormat:'RGBA32'})
   const loadImagePromises = []

    loadImagePromises.push(loadImage(img1Path))
    loadImagePromises.push(loadImage(img2Path))

    Promise.all(loadImagePromises).then(values => {
        ctx.drawImage(values[0], 0, 0, setting.imgOri.dim.w, setting.imgOri.dim.h)
        ctx.drawImage(values[1], setting.imgAdd.pos.x, setting.imgAdd.pos.y, setting.imgAdd.dim.w, setting.imgAdd.dim.h)

        saveCanvaToPngImg(canvas, pathNameOutput)
    })
    .then(function()
    {
        /*
        res.json({'error' : false,
                    'msg' : 'success'})*/
        res.json ({'error' : false,
        'msg' : 'success'})
    })
    .catch(function(){
        return({'error' : true,
                    'msg' : 'merge image error 2'})
    })
}

function saveCanvaToPngImg(canvas, pathName){
    // console.log('<img src="' + canvas.toDataURL() + '" />')
    canvas.toDataURL('image/png', (err, png) => {

    var data = png.replace(/^data:image\/\w+;base64,/, "");
    var buf = new Buffer(data, 'base64');

    fs.writeFileSync(pathName, buf)
  })
}

/*
merge2Img('./img/cat1.png', './img/arrow.png', './target/john.png', {imgOri :
                                                                        {dim :
                                                                            {w : 200, h : 200}
                                                                        },
                                                                     imgAdd :
                                                                        {dim :
                                                                            {w : 50,  h : 50},
                                                                         pos : 
                                                                            {x : 50, y : 150}
                                                                        }
                                                                    })
*/