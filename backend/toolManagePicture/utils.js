
const fs = require('fs')
const {PATH_HOME, PATH_TMP_IMG, PATH_PROFILE_IMG, PATH_SELFIE_IMG, PATH_STICKER_IMG} = require('./setting')

export function createFilesSystemImg(){
    console.log(PATH_SELFIE_IMG)
    fs.mkdir(PATH_HOME, { recursive: true }, (err) => {
        if (err)
            console.log(err)
      })
      
    fs.mkdir(PATH_PROFILE_IMG, { recursive: true }, (err) => {
        if (err)  console.log(err);
      })
    fs.mkdir(PATH_SELFIE_IMG,  { recursive: true }, (err) => {
        if (err)  console.log(err);
      })
    fs.mkdir(PATH_STICKER_IMG, { recursive: true }, (err) => {
        if (err)  console.log(err);
      })
      fs.mkdir(PATH_TMP_IMG, { recursive: true }, (err) => {
        if (err)  console.log(err);
      })
}
 
export function getPathProfileImg(){
    return (PATH_PROFILE_IMG)
}

export function getPathSelfieImg(){
    return (PATH_SELFIE_IMG)
}

export function getPathStickerImg(){
    return (PATH_STICKER_IMG)
}

export function getPathTmp(){
    return (PATH_TMP_IMG)
}

export function getPathSelfieImgUser(username){
    return (PATH_SELFIE_IMG  + username)
}

export function makeDirUserSelfie(username){
    console.log(`username : ${username}`)
    fs.mkdir(getPathSelfieImgUser(username), { recursive: true }, (err) => {
        if (err)  console.log(err);
      })
}