/*
**
constant for anaging filsystem
*/

const PATH_HOME = './img/'
const PATH_PROFILE_IMG = PATH_HOME + 'profile/'
const PATH_SELFIE_IMG = PATH_HOME + 'selfie/'
const PATH_STICKER_IMG = PATH_HOME + 'sticker/'
const PATH_TMP_IMG =     PATH_HOME + 'tmp/'

module.exports = {
    PATH_HOME,
    PATH_PROFILE_IMG,
    PATH_SELFIE_IMG,
    PATH_STICKER_IMG,
    PATH_TMP_IMG
}