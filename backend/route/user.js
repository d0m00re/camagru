const router = require('express').Router()
const express = require('express')
const {queryPerform} = require('./../db/utils/queryPerform')
const pool = require('./../db/connect')//( //'./../../db/connect')
const {jwtSignGenerate, verifyToken} = require('./../jwt/jwt')
const filesystem = require('../toolManagePicture/utils')
const cors = require('cors')

router.all('*', cors())
router.use(express.json())
// hoc function
function updateJSONWithRes(res)
{
  return function(elem){
    res.json(elem.rows)
  }
}
 
function manageUserCheckConnection(res)
{
  return function(elem){
    if (elem.rowCount >= 1)
    {
      const userInfo = {
        username : elem.rows[0].username,
        email : elem.rows[0].email,
        userId : elem.rows[0].userid,
        token : jwtSignGenerate(elem.rows[0]),//(elem.rows[0].username),
        error : false,
        valid : true
      }
      res.json(userInfo)
    }
    else
    {
      console.log("Fail")
      res.status(500).send({error : "wrong id", errorValue:true}) 
    }
  }
}

router.route('/users').get((req, res) => {
    queryPerform(pool, 'SELECT * FROM myuser;', updateJSONWithRes(res))
  })

router.get('/user/:username', function(req, res){
    const username = req.params.username
   
    console.log(`username : ${req.params.username}`)
    if (!username){
      console.log("no username")
      res.status(500).send({error : "No username"})
    }
    else{
      console.log(`Find user ${username}`)
      queryPerform(pool, `SELECT * FROM myuser WHERE myuser.username LIKE '${username}'`, updateJSONWithRes(res))
    }
  })

  router.get('/userid/:id', function(req, res){
    const id = parseInt(req.params.id)

    if (!id){
      res.status(500).send({error : "Id not found"})
    }
    else{
      queryPerform(pool, `SELECT * FROM myuser WHERE myuser.userid = ${id};`, updateJSONWithRes(res))
    }
  })

  // get total like for an user
  router.get('/totalLike/:username', function(req, res){
    const username = req.params.username
    const request = `select count(*) from likesf where likesf.selfieid in (select selfie.selfieid from selfie where userid in (select myuser.userid from myuser where myuser.username like '${username}'));`
    pool
    .query(request)
    .then(result => {
      if(result.rowCount == 1)
        res.json({nblike : result.rows[0].count, error : false})
      else
        res.json({errorMsg : 'wrong user', error : true})
    })
    .catch(e => res.json({resp : e, error : true, errorMsg : 'Fail request'}))
  }) 

  /*
  ** nombre total de commentaire sur les selfie d un utilisateur
  ** total number of commer on user selfie
  */
  router.get('/totalComment/:username', function(req, res){
    const username = req.params.username
    const request = `select count(*) from commentsf where commentsf.selfieid in (select selfie.selfieid from selfie where userid in (select myuser.userid from myuser where myuser.username like '${username}'));`
    pool
    .query(request)
    .then(result => {
      if(result.rowCount == 1)
        res.json({nbcomment : result.rows[0].count, error : false})
      else
        res.json({errorMsg : 'wrong user', error : true})
    })
    .catch(e => res.json({resp : e, error : true, errorMsg : 'Fail request'}))
  }) 

  //select count(*) from selfie where userid in (select userid from myuser where username like '42');
  router.get('/totalSelfie/:username', function(req, res){
    const username = req.params.username
    const request = `select count(*) from selfie where userid in (select userid from myuser where username like '${username}');`
    pool
    .query(request)
    .then(result => {
      if(result.rowCount == 1)
        res.json({nbselfie : result.rows[0].count, error : false})
      else
        res.json({errorMsg : 'wrong user', error : true})
    })
    .catch(e => res.json({resp : e, error : true, errorMsg : 'Fail request'}))
  })

  //router.post('/checkConnect2', function(req, res) {
  router.route('/checkConnect2').post((req, res) =>
  {
      const username = req.body.username
      const password = req.body.password

      queryPerform(pool, `SELECT * FROM myuser WHERE myuser.username LIKE '${username}' AND myuser.password LIKE '${password}';`, manageUserCheckConnection(res))
  })

  router.post('/add', function(req, res, next){
    const email =req.body.email
    const username = req.body.username
    const password = req.body.password

    if (!email || !username || !password){
      res.status(500).send({error : "Not enought argument"})
    }
    else{      
    //  queryPerform(pool, `INSERT INTO myuser (email, username, password) VALUES ('${email}', '${username}', '${password}');`, updateJSONWithRes(res))
      pool
      .query(`INSERT INTO myuser (email, username, password) VALUES ('${email}', '${username}', '${password}');`)
      .then(result => {
        //(result.rowCount == 1)
          filesystem.makeDirUserSelfie(username)
          res.json({err : false})
       // else
          //res.json({errorMsg : 'wrong user', error : true})
      })
      .catch(e =>  {
        res.status(500).json({err : true, errMsg : 'User already present'})
      })
    }
  })

/*
** UPDATE table
** SET colonne_1 = 'valeur 1', 
** colonne_2 = 'valeur 2', colonne_3 = 'valeur 3'
** WHERE condition
*/
router.post('/update', function(req, res, next){
  const id = req.body.id
  const email =req.body.email
  const username = req.body.username
  const password = req.body.password

  if (!email || !username || !password || !id){
    res.status(500).send({error : "Not enought argument"})
  }
  else{
    queryPerform(pool, `UPDATE myuser SET
                        email='${email}', username='${username}', password='${password}'
                        WHERE userid=${id}`,updateJSONWithRes(res))
  } 
})
  module.exports = router