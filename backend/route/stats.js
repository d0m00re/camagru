/*
** different api endpoint for getting statistics
*/
const express = require('express');
const router = require('express').Router();
const client = require('./../db/connect')
const cors = require('cors');

router.use(express.json());
router.all('*', cors());

const sqlStatsGallerie = "select (select count(*) from myuser) as nbUser,\
                                (select count(*) from commentsf) as nbComment,\
                                (select count(*) from likesf) as nbLike,\
                                (select count(*) from selfie) as nbSelfie;"

const sqlUserTotalLike = "select count(*) from likesf where likesf.selfieid in (select selfie.selfieid from selfie where userid in (select myuser.userid from myuser where myuser.username like $1))"
const sqlUserTotalComment = "select count(*) from commentsf where commentsf.selfieid in (select selfie.selfieid from selfie where userid in (select myuser.userid from myuser where myuser.username like $1))"
const sqlUserTotalSelfie = "select count(*) from selfie where userid in (select userid from myuser where username like $1)"
const sqlStatsUser = `SELECT \
                        (${sqlUserTotalLike}) as nbLike,\
                        (${sqlUserTotalComment}) as nbComment,\
                        (${sqlUserTotalSelfie}) as nbSelfie;`;

/*
** get gallerie statistics nb like, nb comment, and nb selfie
*/

router.get('/gallerie', function (req, res) {
    client
        .query(sqlStatsGallerie)
        .then(results => {
            let result = results.rows;
            res.json({ ...result[0] })
        })
        .catch(e => res.status(404).json({ error: 'stats gallerie' }))
})

router.get('/user/:username', function (req, res) {
    console.log(sqlStatsUser)
    console.log(req.params)

    const query = {
        name: 'sql-stats-user',
        text: sqlStatsUser,
        values: [req.params.username]
    }

    client
        .query(query)
        .then(result => {
            res.json({ ...result.rows[0] })
        })
        .catch(err => {
            console.log(err)
            res.json({ err })
        })
})

module.exports = router;