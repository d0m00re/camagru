
const { queryPerform, queryPerformErr } = require('../db/utils/queryPerform')
const router = require('express').Router()
const express = require('express')
const pool = require('./../db/connect')
const { verifyToken, verifyTokenSelfieOwn} = require('../jwt/jwt')

const apiLink = require('../apiLink')

const selfie = require('../db/models/selfie')
const like = require('../db/models/like')

const { extractEndPath } = require('../db/utils/pathExtract')

const cors = require('cors')
router.use(express.json())
router.all('*', cors())

// hoc function
function updateJSONWithRes(res) {
  return function (elem) {
    res.json(elem.rows)
  }
}

//hoc function with message
function updateResJSONWithMsg(res, msg) {
  return function (elem) {
    res.json(msg)
  }
}

function updateJSONWithError(res) {
  return function (elem) {
    console.log('request error')
    res.json({ 'error': 'fuck you' })
  }
}

// {exist : true/false, numberRecord : x}
// return exist : false if no record
// else true
function tableExistOrNot(res) {
  return function (elem) {
    let exist = elem.rowCount != 0
    res.json({ 'exist': exist, nbRecord: elem.rowCount })
  }
}

/*
**
*/
router.get('/get/all', function (req, res) {
  const sqlReq = 'select * from Selfie;'

  queryPerform(pool, sqlReq, updateJSONWithRes(res))
})

//-----------------------
/*
** return json tab
** {
**  getImg
**  likeImg
**  fav
**  comment ???
}
*/
// /img/selfie/get/:username/:image
router.get('/get/apiRoute', function (req, res) {
  const apiGetUrl = apiLink.get_selfie_img
  const sqlReq = selfie.SELECT_ALL_SELFIE_TABLE
  const tabApiReq = []

  queryPerform(pool, sqlReq, (resSql => {
    console.log(resSql)

    for (let i in resSql.rows) {
      let pathApiGetSelfie = apiGetUrl + extractEndPath(resSql.rows[i].imgpath)
      tabApiReq.push({ "pathApiGetSelfie": pathApiGetSelfie, "selfieid": resSql.rows[i].selfieid })
    }
    console.log(tabApiReq)
    res.json(tabApiReq)
  }))
})

/*
** mini pager | need manage max
*/
router.get('/get/selfies/:page', function (req, res) {
  const page = req.params.page
  const apiGetUrl = apiLink.get_selfie_img
  //const sqlReq = selfie.SELECT_ALL_SELFIE_TABLE
  const sqlReq = `select * from selfie LIMIT 10 OFFSET ${page * 10};`
  const tabApiReq = []

  console.log(sqlReq)
  queryPerform(pool, sqlReq, (resSql => {
    console.log(resSql)

    for (let i in resSql.rows) {
      let pathApiGetSelfie = apiGetUrl + extractEndPath(resSql.rows[i].imgpath)
      tabApiReq.push({ "pathApiGetSelfie": pathApiGetSelfie, "selfieid": resSql.rows[i].selfieid })
      //tabApiReq.push((resSql[i]).imgName)
    }
    res.json(tabApiReq)
  }))
})


router.get('/get/selfieuser/:username', function (req, res) {
  //  const apiGetUrl = selfie.SELECT_USER_SELFIE_TABLE
  const apiGetUrl = apiLink.get_selfie_img
  //  const sqlReq = selfie.SELECT_ALL_SELFIE_TABLE
  const username = req.params.username

  const tabApiReq = []

  const query = {
    name: 'select-user-selfie-table',
    text: selfie.SELECT_USER_SELFIE_TABLE,
    values: [username],
  }

  queryPerform(pool, `SELECT * from selfie where selfie.userid in (select myuser.userid from myuser where myuser.username like '${username}');`, (resSql => {
    console.log(resSql)


    for (let i in resSql.rows) {
      let pathApiGetSelfie = apiGetUrl + extractEndPath(resSql.rows[i].imgpath)
      tabApiReq.push({ "pathApiGetSelfie": pathApiGetSelfie, "selfieid": resSql.rows[i].selfieid })
      //tabApiReq.push((resSql[i]).imgName)
    }
    console.log(tabApiReq)
    res.json(tabApiReq)
  }))
})

/*
** manage like/unlike
*/ 
router.post('/action/setUnset/like/', verifyToken, function (req, res) {
  const userid = parseInt(req.tokenDecoded.userid)
  const selfieid = parseInt(req.body.selfieid)

  const queryExist = {
    name: 'check-exist',
    text: like.SELECT_ONE_LIKE,
    values: [selfieid, userid]
  }

  const queryAdd = {
    name: 'add-like',
    text: like.INSERT_ONE_LIKE,
    values: [selfieid, userid],
  }

  const queryDelete = {
    name: 'delete-like',
    text: like.DELETE_ONE_LIKE,
    values: [selfieid, userid],
  }

  pool
    .query(queryExist)
    .then(result => {
      if (result.rowCount == 0) {
        console.log({ action: 'go create the table' })
        queryPerformErr(pool, queryAdd, updateResJSONWithMsg(res, { 'action': 'create like table', error: null }), updateResJSONWithMsg(res, { 'action': null, 'error': 'on create like table' }))
      }
      else {
        console.log({ action: 'go delete the table' })
        queryPerformErr(pool, queryDelete, updateResJSONWithMsg(res, { 'action': 'delete like table', 'error': null }), updateResJSONWithMsg(res, { 'action': null, 'error': 'on delete like table' }))
      }
    })
    .catch(err => console.log('roror'))

})

// get one like
router.get('/get/likes/:selfieid/:userid', function (req, res) {
  const userid = req.params.userid
  const selfieid = req.params.selfieid

  const query = {
    // give the query a unique name
    name: 'fetch-like',
    text: like.SELECT_ONE_LIKE,
    values: [selfieid, userid],
  }
  queryPerformErr(pool, query, tableExistOrNot(res), updateJSONWithError(res))
})

router.get('/get/ulikes/:selfieid/:username', function (req, res) {
  const selfieid = req.params.selfieid
  const username = req.params.username

  //.query(query)
  pool
    .query(`SELECT * FROM likesf WHERE likesf.selfieid = ${selfieid} AND likesf.userid  IN (select myuser.userid from myuser where myuser.username like '${username}');`)
    .then(result => {
      if (result.rowCount == 1)
        res.json({ 'exist': result.rowCount == 1, nbRecord: result.rowCount, error: false })
      else
        res.status(404).json({ errorMsg: 'wrong user', error: true })
    })
    .catch(e => res.json({ resp: e, error: true, errorMsg: 'Fail request' }))
})

router.get('/get/like/all', function (req, res) {
  const reqSql = like.SELECT_ALL

  queryPerform(pool, reqSql, updateJSONWithRes(res))
  // res.json({'resp' : 'coucou'}) 
})

router.get('/get/comment/all', function (req, res) {
  const reqSql = comment.SELECT_ALL

  queryPerform(pool, reqSql, updateJSONWithRes(res))
})

/*
** add jwt verification
** delete a selfie
**     0) user verification
**     1) delete file |
**     2) delete all like table associate
**     3) delete the selfie table
**    delete associate like : select * from likesf
-**
** delete comment and like associate
*/
const sqlDeleteSelfieLikes = "DELETE FROM likesf where selfieid=$1;"
const sqlDeleteSelfieComments = "DELETE FROM commentsf where selfieid=$1;"

/*
** delete selfie
*/
const sqlDeleteSelfie = "DELETE FROM selfie where selfieid=$1;"

/*
** delete a selfie
*/
router.delete('/delete/', verifyTokenSelfieOwn, function (req, res) {

  const userid = req.tokenDecoded.userid;

  const selfieid = parseInt(req.body.selfieId)
  const queryDeleteSelfieLikes = {
    name: 'sql-delete-selfie-likes',
    text: sqlDeleteSelfieLikes,
    values: [selfieid]
  }

  const queryDeleteSelfieComment = {
    name: 'sql-delete-selfie-comment',
    text: sqlDeleteSelfieComments,
    values: [selfieid]
  }

  const queryDeleteSelfie = {
    name: 'sql-delete-selfie',
    text: sqlDeleteSelfie,
    values: [selfieid]
  }

  // check if the user could delete this selfie (maybe need pass that in specific jwt function)
        pool.query(queryDeleteSelfieComment)
          .then(success => {
            pool.query(queryDeleteSelfieLikes)
              .then(succes => {
                console.log('Perform request delete selfie')
                pool.query(queryDeleteSelfie)
                  .then(success => {
                    res.json({ success: 'succes deletion like and selfie' })
                  })
                  .catch(err => {
                    res.status(404).json({ error: 'error delete selfie' })
                  })
              })
              .catch(err => {
                res.status(404).json({ error: 'error delete selfie likes' })
              })
          })
})

module.exports = router