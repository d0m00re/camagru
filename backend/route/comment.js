const router = require('express').Router();
const express = require('express');

const comment = require('./../db/models/comment');
const client = require('./../db/connect');

const { verifyToken } = require('../jwt/jwt');
const {queryPerform, queryPerformErr} = require('../db/utils/queryPerform');

const cors = require('cors');

router.use(express.json())
router.all('*', cors())

// hoc function
function updateJSONWithRes(res)
{
  return function(elem){
    res.json(elem.rows);
  }
}

function updateJSONWithError(res){
  return function(elem){
    console.log('request error');
    res.json({'error' : 'bad'});
  }
}

//routes

router.get('/get/all', function (req, res){
    const reqSql = comment.SELECT_ALL;
  
    queryPerform(client, reqSql, updateJSONWithRes(res));
})

router.get('/get/selfie/:selfieid', function(req, res){
    const selfieid = parseInt(req.params.selfieid);

    const query = {
        name : 'get-comments-selfie',
        text : comment.GET_SELFIE_COMMENTS,
        values : [selfieid]
    }
    queryPerformErr(client, query, updateJSONWithRes(res), updateJSONWithError(res));
})

/*
** add a comment on a selfie
*/
router.post('/add', verifyToken, function(req, res){
    console.log(` Add a comment : ${req.body.comment}`);

    const userid =   parseInt(req.tokenDecoded.userid);
    const selfieid = parseInt(req.body.selfieid);
    const mycomment =  req.body.comment;

    const query = {
        name : 'add-comment',
        text : comment.ADD_COMMENT,
        values : [userid, selfieid, mycomment]
    };
    queryPerformErr(client, query, updateJSONWithRes(res), updateJSONWithError(res));
})

module.exports = router