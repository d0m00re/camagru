const { queryPerform } = require('../db/utils/queryPerform')
const router = require('express').Router()
const express = require('express')

const { verifyToken, verifyTokenSelfieOwn} = require('../jwt/jwt')

const pool = require('./../db/connect')
const fs = require('fs')
const { forgeSqlAddPaquet } = require('../db/models/selfie')
const apiLink = require('../apiLink')

var formidable = require('formidable');

//gestion de la structure pour save les images
const filesystem = require('../toolManagePicture/utils')

const cors = require('cors')

const sticker = require('../db/models/sticker')

const mergePicture = require('../toolManagePicture/mergePicture')

const selfie = require('./../db/models/selfie')

router.all('*', cors())
router.use(express.json())

// hoc function
function updateJSONWithRes(res) {
  return function (elem) {
    res.json(elem.rows)
  }
}

/* 
http://localhost:4444/img/selfie/get/toor/1583169374376-hypothesis.png
selfie selfie management
 apiLink.api_get_selfie_img
 img/selfie/get/:username/:image
*/
router.get('/selfie/get/:username/:image', function (req, res) {
  const username = req.params.username
  const image = req.params.image
  const path = filesystem.getPathSelfieImg() + username + '/' + image

  res.sendFile(path, { root: __dirname + '/../' })
})

/*
** get selfie img with id of the selfie
*/
router.get('/get/selfie/:selfieid', function (req, res) {
  const selfieid = parseInt(req.params.selfieid)

  const queryGetSelfie = {
    name: 'get_selfie',
    text: selfie.SELECT_ONE_SELFIE_WT_ID,
    values: [selfieid]
  }

  pool
    .query(queryGetSelfie)
    .then(result => {
      res.sendFile(result.rows[0].imgpath, { root: __dirname + '/../' })
    })
    .catch(err => {
      console.log(err)
      res.json({ 'error': 'request error' })
    })
})

// load une image
// get imag from post
// 1) save img in a tmp rep
// 2) moove in our correct directories
//router.post('/upload', verifyUserIdToken, function (req, res) {
router.post('/upload', verifyToken, function (req, res) {

  console.log("--- UPLOAD IMAGE ----")

  let username = req.tokenDecoded.username;
  let tmpPath = filesystem.getPathTmp()
  let finalPath = null
  let filename = null
  let time = Date.now() + '-'
  let userId = req.tokenDecoded.userid;

  new formidable.IncomingForm().parse(req)
    .on('fileBegin', (name, file) => {
      filename = time + file.name
      tmpPath = tmpPath + (filename)
      file.path = tmpPath
    })
    .on('file', (name, file) => {
    })
    .on('aborted', () => {
    })
    .on('error', (err) => {
      console.error('Error', err)
      throw err
    })
    .on('end', () => {
      console.log('end upload')
      if (filename == null) {
        console.log('error : filename is null')
        res.status(404).json({ 'error': 'no file name' })
      }
      else {
        finalPath = filesystem.getPathSelfieImgUser(username) + '/' + filename
        console.log(`Final path : ${finalPath}`)
        fs.rename(tmpPath, finalPath, function () { })

        // maintenant n peut save les info dans notre table sql :)
        console.log('info to save')
        const dataImageSelfie = {
          userId: userId,
          imgPath: finalPath,
          imgName: filename
        }

        const sqlReq = forgeSqlAddPaquet(dataImageSelfie)
        console.log(`sql req ---> ${sqlReq}`)
        console.log(dataImageSelfie)
        pool
          .query(sqlReq)
          .then(resQuery => {
            res.json({ 'selfieid': resQuery.rows[0].selfieid, 'apiReq': apiLink.server + apiLink.get_selfie_img + username + '/' + filename })
          })
          .catch(e => {
            console.error(e.stack)
            res.status(400).json({ 'error': 'upload img error' })
          })
      }
    })
})

/*
** merge img
** get 2 img
** modif image
** update the original table

** json
** idOriginalImage
** idStickers
** pos : {x, y}
** dim : {x, y}
*/
router.post('/merge', function (req, res) {
  const reqPromeses = []

  // get selfie   img path
  const reqSelfi = `SELECT * FROM Selfie WHERE selfieid = $1;`
  reqPromeses.push(pool.query(reqSelfi, [req.body.idSelfie]))

  // get original img path
  const reqSticker = `SELECT * FROM Sticker WHERE StickerId = $1;`
  reqPromeses.push(pool.query(reqSticker, [req.body.idStickerSelect]))

  Promise.all(reqPromeses).then(function (value) {
    const resp = mergePicture.merge2Img(value[0].rows[0].imgpath,
      value[1].rows[0].imgpath,
      value[0].rows[0].imgpath,
      req.body.setting, res)

  })
    .then(function (v) {
      res.json({ good: 'ok' })
    })
    .catch(err => console.log(err))
})

router.post('/update', verifyTokenSelfieOwn ,function (req, res) {
  console.log("--- update IMAGE ----")

  let username = null
  let finalpath = null
  let filename = null
  let userid = -1
  let idselfie = -1
  let token = -1
  let path = ''
  let tmpPath = filesystem.getPathTmp() + 'temp.jpg'

try {
  new formidable.IncomingForm().parse(req)
    .on('field', (name, field) => {
      console.log(`--- Field : ${name} ${field}`)
      if (name === 'idselfie') {
        idselfie = field
      }
    })
    .on('fileBegin', (name, file) => {
      console.log('file begining save temp file : ' + tmpPath)
      file.path = tmpPath
    })
    .on('file', (name, file) => {
      console.log(`Uploaded file : ` + name)
    })
    .on('aborted', () => {
      console.error('Request aborted by the user')
    })
    .on('error', (err) => {
      console.error('Error', err)
      throw err
    })
    .on('end', () => {
      console.log('end : ')
      const reqSelfie = `SELECT * FROM Selfie WHERE selfieid = $1;`
      const query = {
        name: 'getSelfiePath',
        text: reqSelfie,
        values: [parseInt(idselfie)]
      }
      pool.query(query, (err, result) => {
        path = (result.rows[0].imgpath)
        console.log(`Moove ${tmpPath} to ${path}`)
        fs.rename(tmpPath, path)
        res.json({success : true})
      })
    })
  }
  catch {
    res.statsu(404).json({succes : false})
  }
})

//http://localhost:4444/image/sticker/poussin.png
router.get('/sticker/:name', function (req, res) {
  const name = req.params.name
  const path = filesystem.getPathStickerImg() + name

  console.log(`path : ${path}`)
  res.sendFile(path, { root: __dirname + '/../' })
})

/*
** return json with all stickers tables
*/
router.get('/stickers', function (req, res) {
  const reqSql = sticker.SELECT_ALL_STICKER_IMG

  queryPerform(pool, reqSql, updateJSONWithRes(res))
})

module.exports = router