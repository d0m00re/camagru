/*
** basic programme for create table of camagrudb 
*/

const pool = require('./../backend/db/connect')
const generateDbString = require('./../backend/db/utils/generateDbString')
const express = require('express');


var app = express();

app.set('port', process.env.PORT || 4000);

function test(i){
    console.log(i)
}

function performReq(sqlReq, displayResult) {

    console.log('run query : ')
   // console.log(sqlReq)
    pool.query(sqlReq, function (err, result) {
        console.log('execute query of database creation table')
        if (err) {
            console.log(`error : ${err}`);
        }
        else {
            console.log(`Success : request num`)
 
        }
    })
}

function performReqTab(sqlReq) {
    for (let i in sqlReq) {
        console.log('turn.')
        console.log(sqlReq[i])
       // performReq(sqlReq)

        
        pool.query(sqlReq[i], function (err, result) {
            console.log('execute query of database creation table')
            if (err) {
                console.log(i)
                console.log(err);
            }
            else {
                console.log(`Success : request num ${i}`)
            }
        });
    }
}

function performCreateTable(sqlReq){
    pool.query(sqlReq[0])
    .then(function() {
        console.log('+++++ schema create MyUser');
        return;
    })

    .catch((err) => {
        console.log(err);
        console.log('----- schema could not be initialized :(');
        return;
    })

    .then(pool.query(sqlReq[1]))
    .then(function() {
        console.log('+++++ schema create Sticker');
        return;
    })
    .catch((err) => {
        console.log(err);
        console.log('attributeTable could not be initialized :(');
        return;
    })

    .then(pool.query(sqlReq[2]))
    .then(function() {
        console.log('+++++ schema create Selfie');
        return;
    })
    .catch((err) => {
        console.log(err);
        console.log('----- objectGroupTable could not be created :(');
        return;
    })

    .then(pool.query(sqlReq[3]))
    .then(function() {
        console.log('+++++ schema create Like');
        return;
    })
    .catch((err) => {
        console.log(err);
        console.log('----- objectGroupTable could not be created :(');
        return;
    })

    .then(pool.query(sqlReq[4]))
    .then(function() {
        console.log('+++++ schema create Comment');
        return;
    })
    .catch((err) => {
        console.log(err);
        console.log('----- objectGroupTable could not be created :(');
        return;
    });
}



function start() {
    if (process.argv.length == 3) {
        switch (process.argv[2]) {
            case 'create':
                console.log('create : ')
               // performCreateTable(generateDbString.generateDbCreateTab)
               //performReqTab(generateDbString.generateDbCreateTab)
               performReqTab(generateDbString.generateDbCreateTab)

                break;
            case 'addSticker':
                console.log('add sticker : ')
                console.log(generateDbString.generateDbStickerTab)
                performReqTab(generateDbString.generateDbStickerTab)
                break;
            case 'drop':
                console.log('drop')
                //  performCreateTable(generateDbString.generateDbDropTab)
                performReqTab(generateDbString.generateDbDropTab)            
                break;
            case 'checkSticker':
                console.log('check sticker:')
                performReq('select count(*) from sticker;', true)
                break;
            default:
                console.log('bad argument --> usage : [create/addSticker/drop]')
        }
    }
    else {
        console.log('usage : [create/addSticker/drop]')
    }
}

start()
//process.exit()